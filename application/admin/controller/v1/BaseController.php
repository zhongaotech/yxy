<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/4/9
 * Time: 下午5:03
 */

namespace app\admin\controller\v1;
use app\model\Admin;
use think\Controller;
use app\plugins\gmars\rbac\Rbac;
class BaseController extends Controller
{
    protected $user;
    protected $rbac;

    public function __construct()
    {
        parent::__construct();
        $controller = request()->controller();
        $this->assign('controller',$controller);
        $this->rbac=new Rbac();
    }

    /**
     * 判断是否登陆
     */
    public function auth()
    {
        $admin_id=session('user_id','','admin');
        if(is_null($admin_id)){
            if(request()->isAjax()){
                ajax_error('','请先登录');
            }else{
                $this->error('请先登录','/admin/v1/user/login');
            }
        }else{
            $this->user=Admin::get($admin_id);
            $this->assign('user',$this->user);
                /**
             * 权限检查
             */
            $user=Admin::get($admin_id);
            if(!$user->super){
                if(!$this->rbac->can(request()->url())){
                    if(request()->isAjax()){
                        ajax_error('','您没有权限');
                    }else{
                        $this->error('您没有权限');
                    }
                }
            }

        }
    }

    /**
     * 检查方法
     * @param string $method
     */
    public function method($method='get'){
        $is_method="is".ucfirst($method);
        if(!request()->$is_method()){
            if(request()->isAjax()){
                ajax_error('','请求方式此错误');
            }else{
                $this->error('请求方式此错误');
            }
        }
    }

    /**
     * @param $name 房间名
     * @param $chairmanpwd 老师密码
     * @param $roomtype 教室类型
     * @param $assistantpwd 助教密码
     * @param $patrolpwd 巡课密码
     * @param $starttime 开课时间
     * @param $endtime 结束时间
     * @param $confusernum 普通用户数 0:1 对 1 3:1 对多
     * @param int $passwordrequired  房间是否需要普通密码 0:否、1:是
     * @param string $confuserpwd 普通用户密码
     * @param int $videotype 视频分辨率 0：流畅   1：标清 2：高清   3：超清
     * $param int $videoframerate 视频帧率 10,15,20,25,30
     * $param int $autoopenav 自动开启音视频 0: 不自动开启 1:自动开启
     * @return mixed
     */
    public function ex_roomcreate($name,$starttime,$endtime,$confusernum,$roomtype=3,$chairmanpwd='1234567',$assistantpwd='123456',$patrolpwd='12346',$passwordrequired=0,$confuserpwd='123456',$videoframerate=10,$videotype=0,$autoopenav=0)
    {
        $url =config('kky.url');
        $postdata['key'] =config('kky.authkey');  //必填 企业id  authkey
        $postdata['roomname'] = $name; //必填 房间名称
        $postdata['roomtype'] = $roomtype; //必填 教室类型
        $postdata['chairmanpwd'] = $chairmanpwd; //必填 老师密码	必填，4=<长度<=20
        $postdata['assistantpwd'] = $assistantpwd; //必填 助教密码	必填，4=<长度<=20
        $postdata['patrolpwd'] = $patrolpwd; //必填 巡课密码	必填，4=<长度<=20
        $postdata['starttime'] = $starttime;  //必填 房间开始时间戳(精确到秒)
        $postdata['endtime'] = $endtime; //必填 房间结束时间(精确到秒)
        $postdata['passwordrequired'] = $passwordrequired; //选填 房间是否需要普通密码 0:否、1:是
        if($passwordrequired!=0){
            $postdata['confuserpwd'] =$confuserpwd;//房间普通用户密码 passwordrequired = 1 时必填(4=<长度<=10)
        }
        $postdata['confusernum'] = $confusernum; //必填 普通用户数
        $postdata['videotype'] = $videotype; //选填 视频分辨率 0：流畅   1：标清 2：高清   3：超清
        $postdata['videoframerate'] = $videoframerate; //视频帧率 10,15,20,25,30
        $postdata['autoopenav'] = $autoopenav; //视频帧率 10,15,20,25,30

        $resultdata = curl_post($url,$postdata);
        return $resultdata;
    }


    /**
     * @param $serial 房间号
     * @return mixed
     */
    public function ex_roomdelete($serial)
    {
        $url =config('kky.url')."/roomcreate/";
        $postdata['key'] =config('kky.authkey');  //必填 企业id authkey
        $postdata['serial'] =$serial ;//必填 房间号

        $resultdata = curl_post($url,$postdata);
        return $resultdata;

    }


    /**
     * 获取回放
     * @param $serial 房间号
     */
    public function getrecordlist($serial){
        $this->method();
        $this->auth();
        $url =config('kky.url')."/getrecordlist/";
        $postdata['key'] =config('kky.authkey');  //必填 企业id authkey
        $postdata['serial'] =$serial ;//必填 房间号

        $resultdata = curl_post($url,$postdata);
        return $resultdata;
    }


    /**
     * 创建以及加入房间；直接进入房间
     * @param $serial房间号
     * @param $username姓名
     * @param $pid id
     * @return string
     *
     */
    public function ex_entry($serial,$username,$pid)
    {
        $url =config('kky.url')."/entry/";
        $postdata['key'] =config('kky.authkey');  //必填 企业id authkey
        $postdata['serial'] =$serial; //必填 房间号 非0开始的数字串， 请保证房间号唯一
       // $postdata['roomname'] = 'text'; //选填 房间名
        $str = "崔建佳（公主坟测试）";
        $postdata['username'] = urlencode($username); //必填 用户名 用户在房间中显示的名称；使用UTF8编码，特殊字符需使用urlencode转义
        $postdata['usertype'] = 2; //必填，0：主讲(老师 )  1：助教    2: 学员   3：直播用户  4:巡检员
        $postdata['pid'] = $pid; //选填, 第三方系统的用户id；默认值为0
        $time = time(); //当前时间戳
        $postdata['ts'] = $time; //必填，当前GMT时间戳，int格式
        $postdata['auth'] =md5(config('kky.authkey').$time.$postdata['serial'].$postdata['usertype']); //必填，auth值为MD5(key + ts + serial + usertype)其中key为双方协商的接口密钥：默认值为：5NIWjlgmvqwbt494
        //$postdata['chairmanpwd'] = '123456'; //选填 主席密码 默认值为0。格式为：128位AES加密串，加密密钥默认为5NIWjlgmvqwbt494
        $postdata['passwordrequired'] = 0; //选填 房间是否需要普通密码 0:否、1:是
/*        $postdata['userpassword'] = 'b2bc8b4adcf954055bc1c79379157992'; //选填 房间普通用户密码 passwordrequired = 1 时必填(4位以上)。普通用户密码；默认值为0。格式为：128位AES加密串，加密密钥默认为5NIWjlgmvqwbt494*/
//        $postdata['sidelineuserpwd'] = '0'; //直播用密码 选填, 直播用户密码；默认值为0。格式为：128位AES加密串，加密密钥默认为5NIWjlgmvqwbt494
        $postdata['logintype'] = 0; //选填 登录方式 0:客户端   1：表示falsh登录2：表示自动选择，如果安装了客户端，则启动客户端，否则使用flash
        $postdata['domain'] = 'tjwxyd'; //公司域名
        $postdata['invisibleuser'] = 1; //选填 用户是否隐身 1表示隐身， 0表示不隐身
        $postdata['donotauth'] = 1; //选填 用户是否隐身 1表示隐身， 0表示不隐身
//        var_dump($postdata);
        //$this->setRequestProperty("contentType", "UTF-8");
        $url=$url."serial/". $postdata['serial']."/username/".$postdata['username']."/usertype/".$postdata['usertype']."/pid/".$pid."/"."ts/".$postdata['ts']."/auth/".$postdata['auth']."/passwordrequired/0/"."logintype/0/".'domain/tjwxyd/invisibleuser/1/donotauth/1';
        return $url;
    }

    /**
     * 获取房间详情
     * @param $serial 房间号
     * @return mixed
     */
    public function ex_getonlineuser($serial){
        $url =config('kky.url')."/getlogininfo/";
        $postdata['key'] =config('kky.authkey');  //必填 企业id authkey
        $postdata['serial'] =$serial; //必填 房间号 非0开始的数字串， 请保证房间号唯一
        $resultdata = curl_post($url,$postdata);
        return json_decode($resultdata,true);

    }
}