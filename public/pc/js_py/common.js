// tab切换
$(document).ready(function(e) {

    //tab切换通用；(.tab必须;添加tt属性;qh切换出总的)----------------
    $(".edu_content li").click(function() {
        //改样式
        $(this).addClass("active").siblings().removeClass("active");
        //切换出
        var tt = $(this).attr("tt");
        $(".edu_con_right").children("." + tt).show().siblings().hide();
    });
    //--------------------
    // 微信指向
    $(".edu_wx_show").mouseenter(function() {
        $(".weixin").css({
            "opacity": "1",
            "transition": ".3",
            "z-index": "10"
        });
    });
    $(".edu_wx_show2").mouseenter(function() {
        $(".weixin2").css({
            "opacity": "1",
            "transition": ".3",
            "z-index": "10"
        });
    });
    $(".edu_wx_show").mouseleave(function() {
        $(".weixin").css({
            "opacity": "0",
            "transition": ".3",
            "z-index": "-1"
        });
    });
    $(".edu_wx_show2").mouseleave(function() {
        $(".weixin2").css({
            "opacity": "0",
            "transition": ".3",
            "z-index": "-1"

        });
    });

    // $("#news_list2").mouseover(function(){
    //   $(".newsTitle").css("display","block");
    // },function(){
    //     $("#news_list2").mouseover(function(){
    //         $(".newsTitle").css("display","block");
    //     })
    // });
    $("#news_list2").mouseover(function(){
      $(".newsTitle").css("display","block");
    });
    $("#edu_nav_list").mouseleave(function(){
      $(".newsTitle").css("display","none");
    });
    $("#news_list2").mouseleave(function(){
      $(".newsTitle").css("display","none");
    });
    $(".newsTitle").mouseover(function(){
      $(".newsTitle").css("display","block");
    });

    $(".newsTitle").mouseleave(function(){
      $(".newsTitle").css("display","none");
    });


    $("#name_list").click(function(){
      $(".login_out").css("display","block");
    })
    $(".login_out").mouseleave(function(){
      $(".login_out").css("display","none");
    });
});
