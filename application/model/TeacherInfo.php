<?php
/**
 * User: pengyue
 * Date: 2018/6/5
 */
namespace app\model;
use think\Model;

class TeacherInfo extends Model
{   
    protected $table = 'yxy_teacher_info';

    public function getIdCardImgAttr($value){
        return unserialize($value);
    }


    public function getProvince(){
        return $this->belongsTo(Province::class,'province_code','code');
    }

    public function getCity(){
        return $this->belongsTo(City::class,'city_code','code');
    }


    public function getArea(){
        return $this->belongsTo(Area::class,'area_code','code');
    }
}