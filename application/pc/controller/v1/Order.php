<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/5/25
 * Time: 上午9:24
 */

namespace app\pc\controller\v1;
use app\model\Course;
use app\model\ClassHour;

use app\plugins\alipay\AopClient;
use app\plugins\alipay\request\AlipayTradePagePayRequest;
use app\pc\controller\BaseController;
use EasyWeChat\Foundation\Application;
use app\model\UserCourse;
use Db;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\ErrorCorrectionLevel;
class Order extends BaseController
{
    public function desc(){
        $this->auth();    //判断用户是否登录
        $this->TeacherAuth(false);
        $this->method('get');   //限制提交方式
        $data = request()->get();

        //获取当前用户信息
        $user_data = $this->user;
        //实例化订单模型
        $order = new \app\model\Order();
        //生成订单号
        if(empty($data['order_no'])){
            $order_number = order_number();
        }else{
            $order_number = $data['order_no'];

        }
        if(empty($data['order_no'])){
            if(!empty($data['course_id'])){
                $course =  Course::where('id',$data['course_id'])->find();
                $order->course_id = $data['course_id'];
                if(empty($course['activity_price'])){
                    $order->price = $course['price'];
                }else{
                    $order->price = $course['price']-$course['activity_price'];
                    $order->activity_price = $course['activity_price'];
                }
            }else{
                $order->class_hour_id = $data['class_hour_id'];
                $hour = ClassHour::where('id',$data['class_hour_id'])->find();

                if(empty($hour['activity_price'])){
                    $order->price = $hour['price'];
                }else{
                    $order->price = $hour['price']-$hour['activity_price'];
                    $order->activity_price = $hour['activity_price'];
                }
            }

            //组装插入的数据
            $order->order_no = $order_number;
            $order->user_id = $user_data->id;
            $order->user_name = $user_data->username;
            $order->user_phone = $user_data->phone;
            $order->status = 0;
            $state = $order->save();
            if(!$state){
                $this->error('报名失败');
            }
        }

        $order=\app\model\Order::where("order_no",$order_number)->find();
        return view("",compact('order'));
    }

    public function payment(){

        $this->method('get');   //限制提交方式
        //接收订单号
        $order_no = request()->get('order_no');
        $order_data = \app\model\Order::where('order_no',$order_no)->find();
        $price = $order_data['price'];
        $appid=config("alipay.appId");
        //1为支付宝支付 2为微信支付
        if(request()->get('type')==2){
            $aop = new AopClient();
            $request = new AlipayTradePagePayRequest();
            $request->setNotifyUrl('http://www.yuexueu.com/pc/v1/order/order_state_alipay');
            $request->setReturnUrl('http://www.yuexueu.com/pc/v1/member/order');

            $price=sprintf("%.2f",$price);
            $biz=[
                'body'=>"购买课程",
                'subject'=>"悦学优",
                'out_trade_no'=>$order_no,
                'timeout_express'=>'90m',
                'total_amount'=>"0.01",
                'product_code'=>'FAST_INSTANT_TRADE_PAY',
                'seller_id'=>''
            ];
            $request->setBizContent(json_encode($biz));
            $result = $aop->pageExecute ($request,'get');
            ajax_success($result,'成功');

            // $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
            // dump($result);

            // $resultCode = $result->$responseNode->code;
            // if(!empty($resultCode)&&$resultCode == 10000){
            //     ajax_success($result,'成功');
            // } else {
            //    ajax_error('','失败');
            // }
        }else{
            $app = new Application(config('wechat.'));

            $payment =$app->payment;
            $attributes = [
                'trade_type'       => 'NATIVE', // JSAPI，NATIVE，APP...
                'body'             => '购买课程',
                'detail'           => '悦学优',
                'out_trade_no'     => $order_no,
                'total_fee'        => 0.01*100, // 单位：分
                'notify_url'       => 'http://www.yuexueu.com/pc/v1/order/order_state_wxpay', // 支付结果通知网址，如果不设置则会使用配置里的默认地址
                // ...
            ];

            $order = new \EasyWeChat\Payment\Order($attributes);
            $result = $payment->prepare($order);
            if ($result->return_code == 'SUCCESS' && $result->result_code == 'SUCCESS'){
                $prepayId = $result->prepay_id;
                $codeUrl = $result->code_url;
                //生成二维码
                $Qr = new QrCode($codeUrl);
                $Qr->setSize(300)
                    ->setWriterByName('png')
                    ->setMargin(10)
                    ->setEncoding('utf-8')
                    ->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH)
                    ->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0])
                    ->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255])
//                ->setLogoPath(ROOT_PATH . 'template/common/images/wxpay.png')
//                ->setLogoWidth(65)
                    ->setValidateResult(false);
                $time=time().".png";
                $Qr->writeFile( "./uploads/".$time);



                $qrcode="/uploads/".$time;
                ajax_success(compact('qrcode','order_no'));
            }else{
                ajax_error('','调取支付失败');
            }

        }
    }



    public function wx(){
        $code=request()->get('code');
        $order_no=request()->get("order_no");
        $price=request()->get("price");
        return view('',compact('code','order_no','price'));
    }


    /**
     *  订单--支付成功修改订单状态
     * @return string
     */
    public function order_state_alipay(){
        $aop = new AopClient();
        $aop->rsaCheckV1($_POST,null,'RSA2');
        if($_POST['trade_status']=='TRADE_SUCCESS'){
            // file_put_contents('trade_status.txt',$_POST['trade_status']);
            // 启动事务
            Db::startTrans();
            try {
                $date = date('Y-m-d H:i:s',time());
                $state = \app\model\Order::where('order_no', $_POST['out_trade_no'])->find();
                $state1=$state->save(['status'=>1,'pay_time'=>$date,'pay_type'=>1]);
                if($state1){
                    $order_data = \app\model\Order::where('order_no', $_POST['out_trade_no'])->find();
                    $UserCourse = new UserCourse();
                    if(!empty($order_data['course_id'])){
                        $UserCourse->course_id = $order_data['course_id'];
                        Course::get($order_data['course_id'])->inc('study_num');

                    }else{
                        $UserCourse->class_hour_id = $order_data['class_hour_id'];
                    }
                    $UserCourse->user_id = $order_data['user_id'];
                    $UserCourse->save();
                }
                // 提交事务
                Db::commit();
            } catch (\Exception $e) {
                // 回滚事务
                file_put_contents('error.txt',$e->getMessage());
                Db::rollback();
            }
        }
    }

    /**
     *  订单--支付成功修改订单状态
     * @return string
     */
    public function order_state_wxpay(){
        $app = new Application(config('wechat.'));
        file_put_contents('wechat.txt','get');
        $response = $app->payment->handleNotify(function($notify, $successful){
            // 使用通知里的 "微信支付订单号" 或者 "商户订单号" 去自己的数据库找到订单
            $order =$notify->out_trade_no;

            $date = date('Y-m-d H:i:s',time());
            $state = \app\model\Order::where('order_no',$order)->find();
            if(empty($state)){
                return "Order not exist";
            }


            if ($successful) {
                file_put_contents('wechat1.txt','begin');

                // file_put_contents('trade_status.txt',$_POST['trade_status']);
                // 启动事务
                Db::startTrans();
                try {
                    /*                    file_put_contents('wechat2.txt','startTrans');*/

                    $state1=$state->save(['status'=>1,'pay_time'=>$date,'pay_type'=>2]);
                    if($state1){
                        $order_data = \app\model\Order::where('order_no',$order)->find();
                        $UserCourse = new UserCourse();
                        if(!empty($order_data['course_id'])){
                            $UserCourse->course_id = $order_data['course_id'];
                            Course::get($order_data['course_id'])->inc('study_num');

                        }else{
                            $UserCourse->class_hour_id = $order_data['class_hour_id'];
                        }
                        $UserCourse->user_id = $order_data['user_id'];
                        $UserCourse->save();
                    }
                    // 提交事务
                    Db::commit();

                    /*                    file_put_contents('wechat2.txt','commit');*/

                } catch (\Exception $e) {
                    // 回滚事务
                    /*                    file_put_contents('error.txt',$e->getMessage());*/
                    Db::rollback();
                }
            }
            return true;
        });
        $response->send();
    }


    public function show_order(){
        $order_no=request()->get('order_no');
        $app = new Application(config('wechat.'));
        $res=$app->payment->query($order_no);
        if($res->result_code=='SUCCESS' && $res->trade_state=='SUCCESS'){
            ajax_success($res,'成功');
        }
        ajax_error('',"失败");

    }





}