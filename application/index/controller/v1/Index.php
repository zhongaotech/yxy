<?php
namespace app\index\controller\v1;

use app\model\Advice;
use app\model\IndexImage;
use app\model\Reported;
use app\model\Special;
use app\model\Subject;
use app\model\User;
use app\model\Grade;
use app\model\Course;
use app\model\News as NewsModel;
use app\plugins\alipay\AopClient;
use app\plugins\alipay\request\AlipayTradeWapPayRequest;
use app\facade\Jwt;
use Request;
use EasyWeChat\Foundation\Application;
use EasyWeChat\Payment\Order;
use think\Validate;
use Db;
class Index extends BaseController
{
    public function index(){
        $app =new Application(config('wechat.'));
        $payment =$app->payment;
        $attributes = [
            'trade_type'       => 'JSAPI', // JSAPI，NATIVE，APP...
            'body'             => 'iPad mini 16G 白色',
            'detail'           => 'iPad mini 16G 白色',
            'out_trade_no'     => '1217752501201407033233368018',
            'total_fee'        => 5388, // 单位：分
            'notify_url'       => 'http://xxx.com/order-notify', // 支付结果通知网址，如果不设置则会使用配置里的默认地址
            'openid'           => '当前用户的 openid', // trade_type=JSAPI，此参数必传，用户在商户appid下的唯一标识，
            // ...
        ];

        $order = new Order($attributes);
        $result = $payment->pay($order);

//        $aop=new AopClient();
        $request = new AlipayTradeWapPayRequest ();
        $request->setNotifyUrl('请填写您的异步通知地址');
//        $request->setBizContent('{"product_code":"FAST_INSTANT_TRADE_PAY","out_trade_no":"20150320010101001","subject":"Iphone6 16G","total_amount":"88.88","body":"Iphone6 16G"}');




    }

    /**
     * 首页图片
     * @return string
     */
    public function IndexImage(){
        $IndexImage = IndexImage::where('status',1)->order('id','desc')->limit(3)->select();
        if(!$IndexImage->isEmpty()){
            return ajax_success($IndexImage,'获取图片成功');
        }else{
            return ajax_error($IndexImage,'暂无图片信息');
        }
    }

    /**
     * 首页课程推荐
     * weilang 2018/4/11
     * @return string
     */
    public function index_course(){
        $data           = Request::post();
        if(!empty($data)){
            $where['grade'] = $data['grade'];//前端传过来的年级id
            $grade = Grade::where($where)->find();//通过查询年级表获取年级信息
            $index_course = Course::where('grade_id',$grade)->order('id','desc')->limit(3)->select();
            foreach ($index_course as $key => $value) {
                $start_time = strtotime($value['start_time']);
                $index_course[$key]['start_time_md'] = date('n月j日',$start_time);
                $aa = date('w',$start_time);
                if($aa == 0){$index_course[$key]['start_time_w'] = '日';}
                if($aa == 1){$index_course[$key]['start_time_w'] = '一';}
                if($aa == 2){$index_course[$key]['start_time_w'] = '二';}
                if($aa == 3){$index_course[$key]['start_time_w'] = '三';}
                if($aa == 4){$index_course[$key]['start_time_w'] = '四';}
                if($aa == 5){$index_course[$key]['start_time_w'] = '五';}
                if($aa == 6){$index_course[$key]['start_time_w'] = '六';}
                $index_course_start_time1 = date('G:i',$start_time);
                $index_course_start_time2 = date('G:i',$start_time+5400);
                $index_course[$key]['start_time_end'] = $index_course_start_time1.'-'.$index_course_start_time2;
            }
        }else{
            $index_course = Course::order('id','desc')->limit(3)->select();
            foreach ($index_course as $key => $value) {
                $start_time = strtotime($value['start_time']);
                $index_course[$key]['start_time_md'] = date('n月j日',$start_time);
                $aa = date('w',$start_time);
                if($aa == 0){$index_course[$key]['start_time_w'] = '日';}
                if($aa == 1){$index_course[$key]['start_time_w'] = '一';}
                if($aa == 2){$index_course[$key]['start_time_w'] = '二';}
                if($aa == 3){$index_course[$key]['start_time_w'] = '三';}
                if($aa == 4){$index_course[$key]['start_time_w'] = '四';}
                if($aa == 5){$index_course[$key]['start_time_w'] = '五';}
                if($aa == 6){$index_course[$key]['start_time_w'] = '六';}
                $index_course_start_time1 = date('G:i',$start_time);
                $index_course_start_time2 = date('G:i',$start_time+5400);
                $index_course[$key]['start_time_end'] = $index_course_start_time1.'-'.$index_course_start_time2;
            }
        }
        if(!$index_course->isEmpty()){
            return ajax_success($index_course,'获取课程成功');
        }else{
            return ajax_error($index_course,'暂无课程信息');
        }
    }



    /**
     * 首页新闻咨询
     * weilang 2018/4/11
     * @return string
     */
    public function index_news(){
        $index_news = NewsModel::order('id','desc')->limit(5)->select();
        if(!$index_news->isEmpty()){
            return ajax_success($index_news,'获取新闻成功');
        }else{
            return ajax_error($index_news,'暂无新闻信息');
        }
    }

    /**
     * 咨询
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function registration(){
        $this->method('post');
        $data=request()->post();
        $validate=Validate::make([
            'username'=>"require",
            'phone'=>"require",
            'grade'=>'require',
            'subject'=>"require"

        ],[],[
            'username'=>"姓名",
            'phone'=>"手机号",
            'grade'=>"年级",
            'subject'=>"科目"
        ]);

        if(!$validate->check($data)){
            return ajax_error('',$validate->getError());
        }
        $user=User::where('phone',$data['phone'])->find();
        if(!empty($user)){
            return ajax_error('','请不要重复提交');
        }
        $data1['grade']=Grade::where('name',$data['grade'])->value('id');
        $data['subject']=Subject::where('name',$data['subject'])->value('id');
        Db::startTrans();
        try{
            $number_nowday = substr(date('Ymd'),2,6);
            $number_user_max_all= User::max('number');
            $number_user_max    = substr($number_user_max_all,2,6);
            $number = $number_nowday > $number_user_max ? $number_nowday.'00001' : $number_user_max_all+1;
            $user=User::create(['username'=>$data['username'],'type'=>1,'subject'=>$data['subject'],'phone'=>$data['phone'],'number'=>$number]);
            $user->UserInfo()->save($data1);
            Db::commit();
            ajax_success('','添加成功');

        }catch (\Exception $e){
            Db::rollback();
            ajax_error('',$e->getMessage());

        }
    }

    /**
     * 专题
     * @throws \think\exception\DbException
     */
    public function special(){
        $this->method();
        $specials=Special::order("listor")->order('create_time','desc')->paginate();
        if($specials->isEmpty()){
            ajax_error('',"没有数据");
        }
        ajax_success($specials,'获取成功');
    }

    /**
     * 专题详情
     * @param $id
     * @throws \think\exception\DbException
     */
    public function special_show($id){
        $this->method();
        $special=Special::get($id);
        if(empty($special)){
            ajax_error('',"没有数据");
        }

        ajax_success($special,'获取成功');
    }

    /**
     * 咨询
     */
    public function advice(){
        $this->method('post');
        $data=request()->post();
        $validate=Validate::make([
            'username'=>'require',
            'phone'=>'require',
        ],[],[
            'phone'=>"手机号",
            'username'=>"姓名"
        ]);
        if(!$validate->check($data)){
            ajax_error('',$validate->getError());
        }

        if(Advice::create($data)){
            ajax_success('','提交成功');
        }
        ajax_error('','提交失败');
    }


    /**
     * 媒体报道
     * @throws \think\exception\DbException
     */
    public function reported_list(){
        $this->method();
       $reporteds=Reported::order("listor")->order('create_time','desc')->paginate();
        if($reporteds->isEmpty()){
            ajax_error('',"没有数据");
        }

        ajax_success($reporteds,'获取成功');

    }
}
