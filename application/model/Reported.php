<?php
/**
 * Created by PhpStorm.
 * User: weilang
 * Date: 2018/4/11
 * Time: 15:50
 */
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;

class Reported extends Model{
	
    protected $name='reported';
    use SoftDelete;
    protected $deleteTime = 'del_time';

    public function getThumbAttr($value){
        return url($value,'',false,true);
    }


}