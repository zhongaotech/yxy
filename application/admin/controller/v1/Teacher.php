<?php
/**
 * Created by Sublime.
 * User: weilang
 * Date: 2018/6/4
 * Time: 下午3:45
 */

namespace app\admin\controller\v1;
use app\model\ClassHour;
use app\model\Schedule;
use app\model\Special as specialModel;
use app\model\Subject;
use app\model\Teacher as teacherModel;
use app\model\FreeTime;
use app\model\TeacherInfo;

use think\Validate;
use Db;
class Teacher extends BaseController
{

    /**
     * 首页
     */
    public function index(){
        $this->method();
        $this->auth();
        $wheres=array();
        $subjects=Subject::all();
        $wheresInfo=array();
        if(is_numeric(request()->get('type_cate'))){
            $wheres[]=['teacher_cate','=',request()->get('type_cate')];
        }

        if(is_numeric(request()->get('education'))){
            $wheresInfo[]=['education','=',request()->get('education')];
        }

        if(is_numeric(request()->get('teacher_year'))){
            if(request()->get('teacher_year')<6){
                $wheresInfo[]=['teacher_age','=',request()->get('teacher_year')];
            }elseif(request()->get('teacher_year')==6){
                $wheresInfo[]=['teacher_age','between',[6,10]];
            }else{
                $wheresInfo[]=['teacher_age','>',10];

            }

        }


        if(is_numeric(request()->get('grade'))){
            $wheres[]=['grade','=',request()->get('grade')];
        }

        if(is_numeric(request()->get('type'))){
            $wheres[]=['type','=',request()->get('type')];
        }

        if(is_numeric(request()->get('subject'))){
            $wheres[]=['subject','=',request()->get('subject')];
        }

        if(is_numeric(request()->get('mode'))){
            $wheres[]=['mode','=',request()->get('mode')];
        }

        if(is_numeric(request()->get('level'))){
            $wheres[]=['level','=',request()->get('level')];
        }



        $teacher_ids=TeacherInfo::where($wheresInfo)->column('teacher_id');
        if(!empty($wheresInfo)){
            $wheres[]=['id','in',$teacher_ids];

        }
        $teachers=teacherModel::where($wheres)->paginate();
        return view('',compact('teachers','subjects'));
    }



    /**
     * 首页
     */
    public function pay(){
        $this->method();
        $this->auth();

        return view();
    }



    /**
     *  教师管理-新增教师
     *  @return string
     */
    public function add(){
        $this->method();
        $this->auth();
        $subjects=Subject::all();

        
        


        return view('',compact('subjects'));
    }

    /**
     * 添加行为
     */
    public function addPost(){
        $this->method('post');
        $this->auth();
        $data=request()->post();
//        $data['id_card_img']=request()->file('id_card_img');
        $data['certificate']=request()->file('certificate');
//        $data['id_card_img_positive']=request()->file('id_card_img_positive');
//        $data['id_card_img_instead']=request()->file('id_card_img_instead');
        $data['province_code']=isset($data['province'])?$data['province']:'';
        $data['city_code']=isset($data['city'])?$data['city']:'';
        $data['area_code']=isset($data['district'])?$data['district']:'';
        $data['device']=isset($data['device'])?serialize($data['device']):'';
        $data['evaluate']=isset($data['evaluate'])?serialize($data['evaluate']):'';
        $validate=Validate::make([
            'password'=>'require|min:6',
            'level'=>'require',
            'name'=>'require',
            'sex'=>'require',
            'phone'=>'require',
            'grade'=>"require",
            'subject'=>"require",
            'type'=>"require",
            'mode'=>'require',
            'education'=>'require',

        ],[],[
            'password'=>"密码",
            'level'=>"级别",
            'name'=>'名字',
            "sex"=>"性别",
            'phone'=>"手机号",
            'subject'=>"科目",
            'grade'=>"年级",
            'type'=>"授课方式",
            'mode'=>"授课类型",
            'education'=>"学历"
        ]);

        if(!$validate->check($data)){
            ajax_error('',$validate->getError());
        }

        $certificate_re=uploadImageOne('certificate','20',$ext='jpg,png,gif,jpeg',$path='./uploads',$width='600',$height='600');
        if($certificate_re['status']==0){
            $data['certificate']=$certificate_re['message'];
        }
        $id_card_img_positive_re=uploadImageOne('id_card_img_positive','20',$ext='jpg,png,gif,jpeg',$path='./uploads',$width='600',$height='600');
        $id_card_img_instead_re=uploadImageOne('id_card_img_instead','20',$ext='jpg,png,gif,jpeg',$path='./uploads',$width='600',$height='600');
        if($id_card_img_positive_re['status']==0){
            if($id_card_img_instead_re['status']==0){
                $data['id_card_img']=serialize([$id_card_img_positive_re['message'],$id_card_img_instead_re['message']]);
            }else{
                unlink(env('app_path')."public".$id_card_img_positive_re['message']);
            }
        }

        Db::startTrans();
        try{

            //工号生成
            $number_nowday = substr(date('Ymd'),2,6);
            $number_user_max_all= teacherModel::max('number');
            $number_user_now_max= $number_nowday.'00000';
            $number = $number_user_max_all > $number_user_now_max ? $number_user_max_all+1 : $number_nowday.'00001';
            $data['number']=$number;
            $teacher=teacherModel::create($data);
            $teacher->getInfo()->save($data);
            Db::commit();
            ajax_success($teacher,'添加成功');
        }catch (\Exception $e){
            Db::rollback();

            ajax_error('',$e->getMessage());
        }

    }


    public function edit($id){
        $this->method();
        $this->auth();
        $teacher=teacherModel::get($id);
        $subjects=Subject::all();
//        dump($teacher['evaluate']);

        if(empty($teacher)){
            $this->error('没有该教师','/admin/v1/teacher/index');
        }

        return view('',compact('teacher','subjects'));
    }

    public function editPost(){
        $this->method('post');
        $this->auth();
        $id=request()->post('id');
        $teacher=teacherModel::get($id);
        $subjects=Subject::all();


        $data=request()->post();
//        $data['id_card_img']=request()->file('id_card_img');
        $data['certificate']=request()->file('certificate');
//        $data['id_card_img_positive']=request()->file('id_card_img_positive');
//        $data['id_card_img_instead']=request()->file('id_card_img_instead');
        $data['province_code']=isset($data['province'])?$data['province']:'';
        $data['city_code']=isset($data['city'])?$data['city']:'';
        $data['area_code']=isset($data['district'])?$data['district']:'';
        $data['device']=isset($data['device'])?serialize($data['device']):'';
        $data['evaluate']=isset($data['evaluate'])?serialize($data['evaluate']):'';
        $validate=Validate::make([
//            'password'=>'require|min:6',
            'level'=>'require',
            'name'=>'require',
            'sex'=>'require',
            'phone'=>'require',

        ],[],[
//            'password'=>"密码",
            'level'=>"级别",
            'name'=>'名字',
            "sex"=>"性别",
            'phone'=>"手机号"
        ]);

        if(!$validate->check($data)){
            ajax_error('',$validate->getError());
        }

        $certificate_re=uploadImageOne('certificate','20',$ext='jpg,png,gif,jpeg',$path='./uploads',$width='600',$height='600');
        if($certificate_re['status']==0){
            $data['certificate']=$certificate_re['message'];
        }
        $id_card_img_positive_re=uploadImageOne('id_card_img_positive','20',$ext='jpg,png,gif,jpeg',$path='./uploads',$width='600',$height='600');
        $id_card_img_instead_re=uploadImageOne('id_card_img_instead','20',$ext='jpg,png,gif,jpeg',$path='./uploads',$width='600',$height='600');
        if($id_card_img_positive_re['status']==0){
            if($id_card_img_instead_re['status']==0){
                $data['id_card_img']=serialize([$id_card_img_positive_re['message'],$id_card_img_instead_re['message']]);
            }else{
                unlink(env('app_path')."public".$id_card_img_positive_re['message']);
            }
        }

        Db::startTrans();
        try{

            $teacher=teacherModel::get($id);
            $teacher->save($data);
            $teacher->getInfo()->save($data);
            Db::commit();
            ajax_success($teacher,'修改成功');
        }catch (\Exception $e){
            Db::rollback();

            ajax_error('',$e->getMessage());
        }


        if(empty($teacher)){
            $this->error('没有该教师','/admin/v1/teacher/index');
        }

        return view('',compact('teacher','subjects'));
    }

    /**
     *  教师管理-教师基本信息
     *  @return string
     */
    public function tea_content(){
        $this->method();
        $this->auth();
        if(empty(request()->get('id'))){
            $this->error('非法操作');
        }
        $id=request()->get('id');
        $teacher=teacherModel::get($id);
        return view('',compact('teacher'));
    }



    /**
     *  教师管理-教师课程
     *  @return string
     */
    public function tea_course($id){
        $this->method();
        $this->auth();
        $teacher=teacherModel::get($id);
        $wheres[]=['course_id','NotNUll',''];
        $appends=[];
        if(is_numeric(request()->get('type'))){
            switch (request()->get('type')){
                case 0:
                    $wheres[]=['course_id','NotNUll',''];

                    break;
                case 1:
                    $wheres[]=['class_hour_id','NotNUll',''];
                    break;
            }
            $appends['type']=request()->get('type');
        }

        if(!empty(request()->get('teacher_name'))){
            if(is_numeric(request()->get('type'))){
                switch (request()->get('type')){
                    case 0:
                        $ids=\app\model\Course::where('name','like',"%".request()->get('teacher_name')."%")->column('id');
                        $wheres[]=['course_id','in',$ids];
                        break;
                    case 1:
                        $ids=ClassHour::where('name','like',"%".request()->get('teacher_name')."%")->column('id');
                        $wheres[]=['class_hour_id','in',$ids];
                        break;
                }
            }else{
                $ids=\app\model\Course::where('name','like',"%".request()->get('teacher_name')."%")->column('id');
                $wheres[]=['course_id','in',$ids];
            }
            $appends['teacher_name']=request()->get('teacher_name');

        }

        $schedules=Schedule::where($wheres)->paginate();
        $schedules->appends($appends);
        $model=new Schedule();
        return view('',compact('schedules','teacher','model'));
    }



    /**
     *  教师管理-教师空闲时间
     *  @return string
     */
    public function tea_free($id){
        $this->method();
        $this->auth();
        $teacher=teacherModel::get($id);
        
        $arrwhere['teacher_id']=$teacher['id'];
        $mon['week1'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',1]])->select();
        $mon['week2'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',2]])->select();
        $mon['week3'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',3]])->select();
        $mon['week4'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',4]])->select();
        $mon['week5'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',5]])->select();
        $mon['week6'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',6]])->select();
        $mon['week7'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',7]])->select();
        $teacher['mon']=$mon;

        $after['week1'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',1]])->select();
        $after['week2'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',2]])->select();
        $after['week3'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',3]])->select();
        $after['week4'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',4]])->select();
        $after['week5'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',5]])->select();
        $after['week6'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',6]])->select();
        $after['week7'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',7]])->select();
        $teacher['after']=$after;

        $night['week1'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',1]])->select();
        $night['week2'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',2]])->select();
        $night['week3'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',3]])->select();
        $night['week4'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',4]])->select();
        $night['week5'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',5]])->select();
        $night['week6'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',6]])->select();
        $night['week7'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',7]])->select();
        $teacher['night']=$night;

        return view('',compact('teacher'));
    }



    /**
     *  教师管理-教师薪酬管理
     *  @return string
     */
    public function tea_pay(){
        $this->method();
        $this->auth();

        return view();
    }




}