<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/5/25
 * Time: 下午2:58
 */

namespace app\pc\controller\v1;

use app\model\NewsType;
use app\pc\controller\BaseController;
use app\model\News as NewsModel;
use Db;

class News extends BaseController
{
    public function index(){
        $this->auth(false);
        $this->TeacherAuth(false);

        $cate_id=request()->get("cate_id");
        $search=request()->get('search');

        $types=NewsType::all();
        $wheres=[];
        if(!empty($cate_id)){
            $wheres[]=['type','=',$cate_id];
        }
        if(!empty($search)){
            $wheres[]=['title|content','=','%'.$search.'%'];
        }
        $news=NewsModel::where($wheres)->where('status',1)->paginate(10);
        // $tops=Db::query("SELECT * FROM `yxy_news` WHERE id >= (SELECT FLOOR( MAX(id) * RAND()) FROM `yxy_news` ) and  ORDER BY id LIMIT 5");
        // $recommends=Db::query("SELECT * FROM `yxy_news` WHERE id >= (SELECT FLOOR( MAX(id) * RAND()) FROM `yxy_news` ) ORDER BY id LIMIT 5");
        $tops=NewsModel::where($wheres)->where('status',1)->paginate(5);
        return view('',compact('types','news','tops'));

    }

   public function search(){
       $this->auth(false);
       $this->TeacherAuth(false);

       $search=request()->get('search');
        $news=NewsModel::where("title|content","like",'%'.$search.'%')->paginate(1);
        return view('',compact('news'));
    }

    public function show($id){
        $this->auth(false);
        $this->TeacherAuth(false);

        $arrWhere['status']=1;
        $arrWhere['id']=$id;
        $new=NewsModel::where($arrWhere)->find(); 
        if(empty($new)){
            $this->error('没有该文章');
        }
        $news=NewsModel::where('status',1)->paginate(5);
        $type=NewsType::get($new['type']);
        $new['typeName']=$type['name'];
        return view('',compact('new','news'));
    }



}
