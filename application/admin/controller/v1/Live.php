<?php
/**
 * Created by Sublime.
 * User: weilang
 * Date: 2018/6/6
 * Time: 下午3:45
 */

namespace app\admin\controller\v1;
use app\model\Schedule;
use app\model\Course;
use app\model\ClassHour;
use app\model\Teacher;
use app\model\Grade;
use app\model\Subject;

use think\Validate;

class Live extends BaseController
{

    /**
     * 直播管理-上课中
     */
    public function index(){
        $this->method();
        $this->auth();
        $wheres=[];
        $appends=[];
        if(is_numeric(request()->get('grade_id'))){
            $wheres[]=['grade_id','=',request()->get('grade_id')];
            $appends['grade_id']=request()->get('grade_id');

        }

        if(is_numeric(request()->get('status'))){
            $wheres[]=['status','=',request()->get('status')];
            $appends['status']=request()->get('status');

        }


        if(is_numeric(request()->get('subject_id'))){
            $wheres[]=['subject_id','=',request()->get('subject_id')];
            $appends['subject_id']=request()->get('subject_id');

        }

        if(is_numeric(request()->get('mode'))){
            switch(request()->get('mode')){
                case 0:
                    $wheres[]=['course_id','NotNull',''];

                    break;
                case 1:
                    $wheres[]=['class_hour_id','NotNull',''];
                    break;
            }

            $appends['mode']=request()->get('mode');

        }


        if(!empty(request()->get('teacher'))){
            $teacher_ids=Teacher::where('name','like','%'.request()->get('teacher').'%')->column('id');
            $wheres[]=['class_teacher','in',$teacher_ids];
            $appends['teacher']=request()->get('teacher');

        }

        if(!empty(request()->get('student'))){
            $student_ids=Teacher::where('username','like','%'.request()->get('teacher').'%')->column('id');
            $wheres[]=['student_id','in',$student_ids];
            $appends['student']=request()->get('student');

        }

        $lives=Schedule::where($wheres)->paginate();
        $subjects=Subject::all();
        return view('',compact('lives','subjects'));
    }


    public function recordlist($serial){
        $list=json_decode($this->getrecordlist($serial),true);
        if($list['result']!=0){
            $this->error('没有该课程回放');
        }


        return view('',$list);

    }

    /**
     * 或去上课学生
     * @param $serial
     */
    public function student($serial){
        $this->method();
        $students=$this->ex_getonlineuser($serial);
        $ids=[];
        foreach ($students['logininfo'] as $student){
            if(is_numeric($student['userid']) && $student['userroleid']==2){
                $ids[]=$student['userid'];
            }
        }

        $ids=array_unique($ids);
        $users=\app\model\User::whereIn('id',$ids)->select();

        if($users->isEmpty()){
            ajax_error('','没有上课学生');
        }
        ajax_success($users,'获取成功');
    }


    /**
     * 或去上课学生
     * @param $serial
     */
    public function student1($serial){
        $this->method();
        $students=$this->ex_getonlineuser($serial);
        $student=[];
        $ids=[];
        if($students['result']!=-1){
            foreach ($students['logininfo'] as $student1){
                if(is_numeric($student1['userid']) && $student1['userroleid']==2){
                    $student[$student1['userid']]=$student1;
                    $ids[]=$student1['userid'];
                }
            }
        }


        $student=array_unique($student);
        $ids=array_unique($ids);

        ajax_success(compact('student','ids'),'获取成功');
    }


    /**
     * 获取老师
     * @param $serial
     */
    public function teacher($serial){
        $this->method();
        $students=$this->ex_getonlineuser($serial);
        $teacher=[];
        $assistant=[];
        if($students['result']!=-1){
            foreach ($students['logininfo'] as $info){
                if(is_numeric($info['userid']) && $info['userroleid']==0){
                    $teacher=$info;
                }elseif(is_numeric($info['userid']) && $info['userroleid']==1){
                    $assistant=$info;
                }
            }
        }


        $teacher=array_unique($teacher);
        $assistant=array_unique($assistant);



        ajax_success(compact('teacher','assistant'),'获取成功');
    }


    /**
     * @param $serial
     * @throws \think\exception\DbException
     */
    public function status($serial){
        $this->method();

        $schedule=Schedule::where('room_number',$serial)->find();

        if(empty($schedule)){
            ajax_error('','该课程不存在');

        }

        $teacher=$schedule->getTeacher;
        $assistant=$schedule->getAssistant;
        if(!empty($schedule->course_id)){
            $student=$schedule->getCourse->getStudent;
        }else{
            $student[]=$schedule->getStudent;
        }

        ajax_success(compact('teacher','assistant','student'),'获取数据成功');

    }

}