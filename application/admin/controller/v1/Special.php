<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/5/14
 * Time: 下午3:45
 */

namespace app\admin\controller\v1;
use app\model\Special as specialModel;
use think\Validate;

class Special extends BaseController
{
    /**
     * 首页
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function index(){
        $this->method();
        $this->auth();
        $specials=specialModel::order('listor')->order('create_time')->paginate();
        return view('',compact('specials'));
    }


    public function add(){
        $this->method();
        $this->auth();
        return view();
    }


    public function addPost(){
        $this->method('post');
        $this->auth();
        $data=request()->post();
        $validate=Validate::make([
            'title'=>"require",
            'content'=>"require",
        ],[],[
            'title'=>"标题",
            'content'=>"内容"
        ]);
        if(!$validate->check($data)){
            ajax_error('',$validate->getError());
        }

        $imgs=uploadImageOne('picture','10','jpg,png,gif,jpeg','./uploads','660','330');
        if($imgs['status']!=0){
            ajax_error('',$imgs['message']);
        }

        $data['picture']=$imgs['message'];
        if(!specialModel::create($data)){
             ajax_error('','添加失败');
        }

        ajax_success('','添加成功');

    }





    public function edit($id){
        $this->method();
        $this->auth();
        $special=specialModel::get($id);
        return view('',compact('special'));
    }


    public function editPost(){
        $this->method('post');
        $this->auth();
        $data=request()->post();
        $id=request()->post('id');
        $validate=Validate::make([
            'title'=>"require",
            'content'=>"require",
        ],[],[
            'title'=>"标题",
            'content'=>"内容"
        ]);
        if(!$validate->check($data)){
            ajax_error('',$validate->getError());
        }

        $imgs=uploadImageOne('picture','10','jpg,png,gif,jpeg','./uploads','660','330');
        if($imgs['status']==0){
            $data['picture']=$imgs['message'];
        }

        if(!specialModel::update($data,$id)){
            ajax_error('','修改失败');
        }

        ajax_success('','修改成功');

    }
}