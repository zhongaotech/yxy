<?php
/**
 * Created by Subline.
 * User: weilang
 * Date: 2018/6/13
 * Time: 17:26
 */

namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;

class Audition extends Model{
    use SoftDelete;
    protected $deleteTime = 'del_time';
    protected $table='yxy_audition';
}