<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/3/29
 * Time: 下午4:21
 */
use \think\Image;
use app\model\SendSms;

/**
 * 上传单个图片
 * @param string $size
 * @param string $ext
 * @param string $file
 * @param string $width
 * @param string $height
 * @return array status=>状态 message=>错误或路径
 */
function uploadImageOne($fileName,$size='1',$ext='jpg,png,gif,jpeg',$path='./uploads',$width='200',$height='200'){
    $file=request()->file($fileName);
    if(empty($file)){
        return ['status'=>2,'message'=>"请上传图片"];

    }
    $size=$size*1024*1024;
    $info=$file->validate(['size'=>$size,'ext'=>$ext])->move($path);
//    dump($info);
    if($info){
        $path=ltrim($path,'./');
        $saveName=DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR.$info->getSaveName();
        $saveName1=".".DIRECTORY_SEPARATOR.$path.DIRECTORY_SEPARATOR.$info->getSaveName();

        $image=Image::open($saveName1);
        $image->thumb($width,$height)->save($saveName1);
        return ['status'=>0,'message'=>$saveName];

    }else{
        return ['status'=>1,'message'=>$file->getError()];
    }


/*    $file->validate();*/
}

/**
 * ajax返回正确时调用
 * @param array $data
 * @param string $message
 * @return \think\response\Json
 */
function ajax_success($data=[],$message=''){
    $data1['code']=200;
    $data1['status']='success';
    $data1['data']=$data;
    $data1['message']=$message;
    json($data1, 200, ['Access-Control-Allow-Origin'=> '*','Access-Control-Allow-Headers'=>'Content-type,token,x-requested-with','Access-Control-Expose-Headers'=>'*','Access-Control-Allow-Methods'=>"GET,POST,PUT,DELETE,PATCH,OPTIONS"])->send();
    exit();
}

/**
 * 返回错误时调用
 * @param array $data
 * @param string $message
 * @return \think\response\Json
 */
function ajax_error($data=[],$message=''){
    $data1['code']=400;
    $data1['status']='fail';
    $data1['data']=$data;
    $data1['message']=$message;
    json($data1, 200, ['Access-Control-Allow-Origin'=> '*','Access-Control-Allow-Headers'=>'Content-type,token,x-requested-with','Access-Control-Expose-Headers'=>'*','Access-Control-Allow-Methods'=>"GET,POST,PUT,DELETE,PATCH,OPTIONS"])->send();
    exit();
}

/**
 * 订单号生成
 * @return string
 */
function order_number(){
    $time = date('Ymd', time());
    $number = rand(0000,9999);
    $order_number = $time.$number;
    return  $order_number;
}

/**
 * 生存验证码
 * @param int $length
 * @return int
 */
function generate_code($length = 4) {
    $min = pow(10 , ($length - 1));
    $max = pow(10, $length) - 1;
    return mt_rand($min, $max);
}

/**
 * @param $fileName
 * @param bool $max
 * @param string $size
 * @param string $ext
 * @param string $path
 * @param string $width
 * @param string $height
 * @return array status=>状态 message=>错误或路径
 */
function uploadsImg($fileName,$max=false,$size='10',$width='1000',$height='1000',$ext='jpg,png,gif,jpeg',$path='./uploads'){
    $files=request()->file($fileName);
    if(is_numeric($max)){
        if(count($files)>$max){
            return ['status'=>1,'message'=>"超过文件上传数"];

        }
    }
    if(empty($files)){
        return ['status'=>2,'message'=>"请上传文件"];
    }
    $size=$size*1024*1024;
    $response['status']=0;
    foreach($files as $file){
        $info=$file->validate(['size'=>$size,'ext'=>$ext])->move($path);
        if($info){
            $saveName=DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR.$info->getSaveName();
            $saveName1=".".DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR.$info->getSaveName();
            $image=Image::open($saveName1);
            $image->thumb($width,$height)->save($saveName1);
            $response['message'][]=$saveName;
        }
    }

    return $response;

}

/**
 * @param $phone
 * @param int $length
 * @param int $out_time
 * @param int $get_ime
 */
function sendSms($phone,$length=4,$out_time=60,$get_ime=60){
    $code=generate_code($length);

    $sms=SendSms::where("phone",$phone)->order('create_time','desc')->find();
//        dump($sms);
//        exit();
    if(!empty($sms) && strtotime($sms->create_time)+$get_ime>time()){
        ajax_error('',$out_time.'秒内只能获取一次');

    }

     $ch = curl_init();
     // 必要参数
     $apikey = "d0a614ab1e413bb0ef972f42d88fe57f"; //修改为您的apikey(https://www.yunpian.com)登录官网后获取
     $mobile = $phone; //请用手机号代替
     $text="【悦学优】您的验证码是".$code."，如非本人操作，请忽略本短信";
     //将验证码存入数据库
     $data['phone'] = $phone;
     $data['code'] = $code;
     $data['create_time'] = time();
     // 发送短信
     $data=array('text'=>$text,'apikey'=>$apikey,'mobile'=>$mobile);
     curl_setopt ($ch, CURLOPT_URL, 'https://sms.yunpian.com/v2/sms/single_send.json');
     curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
     curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
     $json_data = curl_exec($ch);
     //解析返回结果（json格式字符串）
     $array = json_decode($json_data,true);
     if(!$array){
         ajax_error('','验证码发送失败');

     }
    $user=SendSms::create([
        'phone'=>$phone,
        'code'=>$code,
        'code_out_time'=>date("Y-m-d H:i:s",time()+$out_time),
        'is_use'=>0
    ]);

    if(!$user->id){
        ajax_error('','验证码发送失败');

    }
    ajax_success('','验证码发送成功');
}



/**
 * 保存64位编码图片
 */

function saveBase64ImageOne($base64_image_content,$width='100',$height='200'){

    if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $base64_image_content, $result)){

        //图片后缀
        $type = $result[2];

        //保存位置--图片名
        $image_name = date('His') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT) . "." . $type;
        $image_url1 = '/uploads/' . date('Ymd') . '/' ;
        $image_url = env('root_path') . '/public' . $image_url1;
        $img_1=$image_url.$image_name;
        if (!is_dir(dirname( $image_url))) {
            mkdir(dirname( $image_url));
            chmod(dirname($image_url), 0777);
            umask($oldumask);

        }

        //解码
        $decode = base64_decode(str_replace($result[1], '', $base64_image_content));
        if (file_put_contents($img_1, $decode)) {

            $image = Image::open($img_1);
            $image->thumb($width, $height)->save($img_1);
            $response['status']=0;

            $response['message']=$image_url1.$image_name;
            return $response;

        }
    }else{
        return ['status'=>2,'message'=>'base64图片格式有误'];



    }

}




/**
 * 保存64位编码图片
 */

function saveBase64Images($base64_image_contents,$width='100',$height='200'){
    $response['status']=0;
    $base64_image_contents=request()->post($base64_image_contents);
    if(request()->post($base64_image_contents)){
        return ['status'=>1,'message'=>"无上传图片"];
    }
    foreach($base64_image_contents as $img) {
        if (preg_match('/^(data:\s*image\/(\w+);base64,)/', $img, $result)) {
            //图片后缀
            $type = $result[2];

            //保存位置--图片名
            $image_name = date('His') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT) . "." . $type;
            $image_url1 = '/uploads/' . date('Ymd') . '/' ;
            $image_url = env('root_path') . '/public' . $image_url1;
            $img_1=$image_url.$image_name;
            if (!is_dir(dirname( $image_url))) {
                mkdir(dirname( $image_url));
                chmod(dirname($image_url), 0777);
                umask($oldumask);

            }

            //解码
            $decode = base64_decode(str_replace($result[1], '', $img));
            if (file_put_contents($img_1, $decode)) {

                $image = Image::open($img_1);
                $image->thumb($width, $height)->save($img_1);
                $response['message'][]=$image_url1.$image_name;
            }
        }
    }

    return $response;
}

/**
 * 模拟post请求
 * @param string $url
 * @param string $postdata
 * @param array $options
 * @return mixed
 */
function curl_post($url='', $postdata='', $options=array()){
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    if (!empty($options)){
        curl_setopt_array($ch, $options);
    }
    $data = curl_exec($ch);
    return $data;
}

/**
 * 模拟get请求
 * @param string $url
 * @param array $options
 * @return mixed
 */
function curl_get($url='', $options=array()){
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    if (!empty($options)){
        curl_setopt_array($ch, $options);
    }
    $data = curl_exec($ch);
    curl_close($ch);

    return json_decode($data,true);
}


/*
* 中英混合的字符串截取
* @param unknown_type $sourcestr
* @param unknown_type $cutlength
*/
function cut_str($sourcestr, $cutlength) {
        $sourcestr=htmlspecialchars(strip_tags(str_replace("'","'",str_replace ( '"', '＂', $sourcestr))));
        $returnstr = '';
        $i = 0;
        $n = 0;
        $str_length = strlen ( $sourcestr ); //字符串的字节数
        while ( ($n < $cutlength) and ($i <= $str_length) ) {
            $temp_str = substr ( $sourcestr, $i, 1 );
            $ascnum = Ord ( $temp_str ); //得到字符串中第$i位字符的ascii码
            if ($ascnum >= 224) //如果ASCII位高与224，
            {
                $returnstr = $returnstr . substr ( $sourcestr, $i, 3 ); //根据UTF-8编码规范，将3个连续的字符计为单个字符
                $i = $i + 3; //实际Byte计为3
                $n ++; //字串长度计1
            } elseif ($ascnum >= 192) //如果ASCII位高与192，
            {
                $returnstr = $returnstr . substr ( $sourcestr, $i, 2 ); //根据UTF-8编码规范，将2个连续的字符计为单个字符
                $i = $i + 2; //实际Byte计为2
                $n ++; //字串长度计1
            } elseif ($ascnum >= 65 && $ascnum <= 90) //如果是大写字母，
            {
                $returnstr = $returnstr . substr ( $sourcestr, $i, 1 );
                $i = $i + 1; //实际的Byte数仍计1个
                $n ++; //但考虑整体美观，大写字母计成一个高位字符
            } else //其他情况下，包括小写字母和半角标点符号，
            {
                $returnstr = $returnstr . substr ( $sourcestr, $i, 1 );
                $i = $i + 1; //实际的Byte数计1个
                $n = $n + 0.5; //小写字母和半角标点等与半个高位字符宽...
            }
        }
        if ($str_length > $cutlength) {
            $returnstr = $returnstr . "..."; //超过长度时在尾处加上省略号
        }
        return $returnstr;
    }


/**
 * 下周时间
 * @return array
 */
    function nextTime(){
        $time=[
            strtotime(date('Y-m-d', strtotime("next week Monday", time()))),
            strtotime(date('Y-m-d', strtotime("next week Sunday", time()))) + 24 * 3600 - 1
        ];

        return $time;
    }



/**
 * 创建以及加入房间；直接进入房间
 * @param $serial房间号
 * @param $username姓名
 * @param $pid id
 * @return string
 *
 */
function ex_entry($serial,$username,$pid,$usertype=2,$passwordrequired=0,$userpassword='0')
{
    $url =config('kky.url')."/entry/";
    $postdata['key'] =config('kky.authkey');  //必填 企业id authkey
    $postdata['serial'] =$serial; //必填 房间号 非0开始的数字串， 请保证房间号唯一
    // $postdata['roomname'] = 'text'; //选填 房间名
    $postdata['username'] = urlencode($username); //必填 用户名 用户在房间中显示的名称；使用UTF8编码，特殊字符需使用urlencode转义
    $postdata['usertype'] = $usertype; //必填，0：主讲(老师 )  1：助教    2: 学员   3：直播用户  4:巡检员
    $postdata['pid'] = $pid; //选填, 第三方系统的用户id；默认值为0
    $time = time(); //当前时间戳
    $postdata['ts'] = $time; //必填，当前GMT时间戳，int格式
    $postdata['auth'] =md5(config('kky.authkey').$time.$postdata['serial'].$postdata['usertype']); //必填，auth值为MD5(key + ts + serial + usertype)其中key为双方协商的接口密钥：默认值为：5NIWjlgmvqwbt494
    //$postdata['chairmanpwd'] = '123456'; //选填 主席密码 默认值为0。格式为：128位AES加密串，加密密钥默认为5NIWjlgmvqwbt494
    $postdata['passwordrequired'] = $passwordrequired; //选填 房间是否需要普通密码 0:否、1:是
    $postdata['userpassword']=0;
    if($passwordrequired!=0){
        $postdata['userpassword']=bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $postdata['key'], $userpassword, MCRYPT_MODE_ECB));

    }
//    /*        $postdata['userpassword'] = 'b2bc8b4adcf954055bc1c79379157992'; //选填 房间普通用户密码 passwordrequired = 1 时必填(4位以上)。普通用户密码；默认值为0。格式为：128位AES加密串，加密密钥默认为5NIWjlgmvqwbt494*/
//        $postdata['sidelineuserpwd'] = '0'; //直播用密码 选填, 直播用户密码；默认值为0。格式为：128位AES加密串，加密密钥默认为5NIWjlgmvqwbt494
    $postdata['logintype'] = 0; //选填 登录方式 0:客户端   1：表示falsh登录2：表示自动选择，如果安装了客户端，则启动客户端，否则使用flash
    $postdata['domain'] = 'tjwxyd'; //公司域名
    $postdata['invisibleuser'] = 1; //选填 用户是否隐身 1表示隐身， 0表示不隐身
    $postdata['donotauth'] = 1; //选填 用户是否隐身 1表示隐身， 0表示不隐身
//        var_dump($postdata);
    //$this->setRequestProperty("contentType", "UTF-8");
    $url=$url."serial/". $postdata['serial']."/username/".$postdata['username']."/usertype/".$postdata['usertype']."/pid/".$pid."/"."ts/".$postdata['ts']."/auth/".$postdata['auth']."/passwordrequired/".$postdata['passwordrequired']."/userpassword/".$postdata['userpassword']."/"."logintype/0/".'domain/tjwxyd/invisibleuser/1/donotauth/1';
    return $url;
}



