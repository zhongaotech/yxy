<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/5/16
 * Time: 下午3:11
 */

namespace app\pc\controller;


use app\model\Teacher;
use app\model\User;
use think\Controller;

class BaseController extends Controller
{
    protected $user;
    protected $teacher;
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * 判断是否登陆
     */
    public function auth($flag=true)
    {
//        session('user_id',9,'pc');
        $user_id=session('user_id','','pc');
        if(is_null($user_id)){
            if(request()->isAjax()){
                 if($flag){
                    ajax_error('','请先登录');
                }
            }else{
                if($flag){
                    $this->error('请先登录','/pc/v1/member/login');
                }
            }
            $this->assign('session','');
            $this->assign('user','');


        }else{
            $this->user=User::get($user_id);
            $this->assign('session',$user_id);
            $this->assign('user',$this->user);



        }

    }

    /**
     * 判断老师是否登陆
     */
    public function TeacherAuth($flag=true)
    {
//        session('user_id',9,'pc');
        $user_id=session('teacher_id','','pc');
        if(is_null($user_id)){
            if(request()->isAjax()){
                if($flag){
                    ajax_error('','请先登录');
                }
            }else{
                if($flag){
                    $this->error('请先登录','/pc/v1/member/login');
                }
            }
            $this->assign('session_teacher','');
            $this->assign('teacher','');


        }else{
            $this->teacher=Teacher::get($user_id);
            $this->assign('session_teacher',$user_id);
            $this->assign('teacher',$this->teacher);



        }

    }

    /**
     * 检查方法
     * @param string $method
     */
    public function method($method='get'){
        $is_method="is".ucfirst($method);
        if(!request()->$is_method()){
            if(request()->isAjax()){
                ajax_error('','请求方式此错误');
            }else{
                $this->error('请求方式此错误');
            }
        }
    }
}