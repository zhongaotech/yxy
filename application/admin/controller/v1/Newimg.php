<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/5/7
 * Time: 下午4:32
 */

namespace app\admin\controller\v1;
use app\model\NewImg as NewImgModel;
use app\model\IndexImage;
use think\Validate;

class Newimg extends BaseController
{
    /**
     * 图片列表-教育头条
     * @return \think\response\View
     */
    public function index(){
        $this->method();
        $this->auth();
        $NewImgs=NewImgModel::limit(4)->select();
        // dump($NewImgs);
        return view('',compact('NewImgs'));
    }

    public function edit($id){
        $this->method();
        $this->auth();
        $img=NewImgModel::get($id);
        return view('',compact('img'));
    }

    public function editPost(){
        $this->method('post');
        $this->auth();
        $id=request()->post('id');
        $img=uploadImageOne('img',$size='5',$ext='jpg,png,gif,jpeg',$path='./uploads',$width='1980',$height='1280');
        $data=request()->except('id');
        if($img['status']==0){
            $data['img']=$img['message'];
        }
        // dump($data);die;
        if(!NewImgModel::get($id)->save($data)){
            ajax_error('','修改图片失败');
        }
        ajax_success('','修改图片成功');
    }


    /**
     * 图片列表-首页轮播图
     * @return \think\response\View
     */
    public function banner(){
        $this->method();
        $this->auth();
        $NewImgs=IndexImage::select();;
        return view('',compact('NewImgs'));
    }

    public function edit_banner($id){
        $this->method();
        $this->auth();
        $img=IndexImage::get($id);
        return view('',compact('img'));
    }


    /**
     * 添加banner图
     * @return \think\response\View
     */
    public function add_banner(){
        $this->method();
        $this->auth();
        return view();
    }


    public function add_banner_post(){
        $this->method('post');
        $this->auth();
        $data=request()->post();
        $data['photo']=request()->file('photo');
        $validate=Validate::make([
            'title'=>"require",
            'photo'=>"image",
            'cate'=>"require",
        ],[],[
            'title'=>"标题",
            'photo'=>"图片",
            'cate'=>"分类"

        ]);

        if($validate->check($data)!==true){
            ajax_error($this->user,$validate->getError());
        }

        $re=uploadImageOne('photo','20','jpg,png,gif,jpeg','./uploads','1024','1024');
        if($re['status']!=0){
            ajax_error($this->user,$re['message']);
        }

        $data['photo']=$re['message'];
        $data['type']=1;
        $data['status']=1;

        if(!$a=IndexImage::create($data)){
            ajax_error($this->user,'添加失败');
        }

        ajax_success($a,'添加成功');
    }


    // public function editBannerPost(){
    //     $this->method('post');
    //     $this->auth();
    //     $id=request()->post('id');
    //     $img=uploadImageOne('img');
    //     $data=request()->except('id');
    //     if($img['status']==0){
    //         $data['img']=$img['message'];
    //     }
    //     dump($img);die;
    //     if(!NewImgModel::get($id)->save($data)){
    //         ajax_error('','修改图片失败');
    //     }
    //     ajax_success('','修改图片成功');
    // }


}