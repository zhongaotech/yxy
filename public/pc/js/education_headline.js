// tab切换
$(document).ready(function(e) {

    //tab切换通用；(.tab必须;添加tt属性;qh切换出总的)----------------
    $(".edu_content li").click(function() {
        //改样式
        $(this).addClass("active").siblings().removeClass("active");
        //切换出
        var tt = $(this).attr("tt");
        $(".edu_con_right").children("." + tt).show().siblings().hide();


    });
    //--------------------
    // 微信指向
    $(".edu_wx").mouseenter(function() {
        $(".weixin").css({
            "opacity": "1",
            "transition": ".3"
        });
    });
    $(".edu_wx2").mouseenter(function() {
        $(".weixin2").css({
            "opacity": "1",
            "transition": ".3"
        });
    });
    $(".edu_wx").mouseleave(function() {
        $(".weixin").css({
            "opacity": "0",
            "transition": ".3"
        });
    });
    $(".edu_wx2").mouseleave(function() {
        $(".weixin2").css({
            "opacity": "0",
            "transition": ".3"
        });
    });
});