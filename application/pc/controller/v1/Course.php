<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/5/16
 * Time: 下午7:10
 */

namespace app\pc\controller\v1;


use app\model\ClassHour;
use app\model\Grade;
use app\model\Season;
use app\model\Subject;
use app\model\Course as CourseModel;
use app\pc\controller\BaseController;
use app\model\IndexImage;
class Course extends BaseController
{
    /**
     * 首页
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index(){
        $this->auth(false);
        $this->TeacherAuth(false);

        $IndexImage = IndexImage::where('status',1)->where('cate',1)->limit(3)->select();


        $grades=Grade::order('listor','desc')->select();
        $subjects=Subject::order('listor')->select();
        $seasons=Season::order('listor')->select();

        return view('',compact('grades','subjects','seasons','IndexImage'));
    }

    /**
     * ajax课程
     */
    public function indexAjax(){
        $data=request()->get();
        $wheres=[];
        $orders=[];

        if(!empty($data['grade_id']) && is_numeric($data['grade_id'])){
            $wheres[]=['grade_id','=',$data['grade_id']];
        }
        if(!empty($data['mode']) && is_numeric($data['mode'])){
            $wheres[]=['mode','=',$data['mode']];
        }
        if(!empty($data['type']) && is_numeric($data['type']) && $data['type']==1){
            $courses=ClassHour::where($wheres)->order($orders)->paginate(5,false,['path'=>"javascript:getCours([PAGE])"]);

            return view('',compact('courses'));
        }

        if(!empty($data['subject_id']) && is_numeric($data['subject_id'])){
            $wheres[]=['subject_id','=',$data['subject_id']];
        }

        if(!empty($data['season_id']) && is_numeric($data['season_id'])){
            $wheres[]=['season_id','=',$data['season_id']];
        }



        if(!empty($data['start_time'])){
            $start_time=explode('.',$data['start_time']);
            $orders[$start_time[0]]=$start_time[1];
        }

        if(!empty($data['price'])){
            $price=explode('.',$data['price']);
            $orders[$price[0]]=$price[1];
        }



        $courses=CourseModel::where($wheres)->order($orders)->paginate(5,false,['path'=>"javascript:getCours([PAGE])"]);

        return view('',compact('courses'));

    }


    public function hours(){
        $this->auth(false);
        $this->TeacherAuth(false);
        $grades=Grade::order('listor')->select();
        $subjects=Subject::order('listor')->select();
        $seasons=Season::order('listor')->select();
    }


    public function course_show($id){
        $this->auth(false);
        $this->TeacherAuth(false);

        $course=CourseModel::get($id);
        return view('',compact('course'));

    }

    public function on_line(){
        $this->auth(false);
        $this->TeacherAuth(false);

        $IndexImage = IndexImage::where('status',1)->where('cate',2)->limit(3)->select();


        $grades=Grade::order('listor')->select();
        $subjects=Subject::order('listor')->select();
        $seasons=Season::order('listor')->select();

        return view('',compact('grades','subjects','seasons','IndexImage'));
    }


    public function on_line_ajax(){
        $data=request()->get();
        $wheres[]=['mode','=','1'];
        if(!empty($data['grade_id']) && is_numeric($data['grade_id'])){
            $wheres[]=['grade_id','=',$data['grade_id']];
        }

        if(!empty($data['type']) && is_numeric($data['type']) && $data['type']==1){
            $courses=ClassHour::where($wheres)->paginate('',false,['path'=>"javascript:getCours([PAGE])"]);

            return view('',compact('courses'));
        }


        if(!empty($data['subject_id']) && is_numeric($data['subject_id'])){
            $wheres[]=['subject_id','=',$data['subject_id']];
        }

        if(!empty($data['season_id']) && is_numeric($data['season_id'])){
            $wheres[]=['season_id','=',$data['season_id']];
        }


        $courses=CourseModel::where($wheres)->paginate('',false,['path'=>"javascript:getCours([PAGE])"]);

        return view('',compact('courses'));
    }


    public function under_line(){
        $this->auth(false);
        $this->TeacherAuth(false);


        $IndexImage = IndexImage::where('status',1)->where('cate',3)->limit(3)->select();


        $grades=Grade::order('listor')->select();
        $subjects=Subject::order('listor')->select();
        $seasons=Season::order('listor')->select();

        return view('',compact('grades','subjects','seasons','IndexImage'));
    }


    public function under_line_ajax(){
        $data=request()->get();
        $wheres[]=['mode','=','2'];
        if(!empty($data['grade_id']) && is_numeric($data['grade_id'])){
            $wheres[]=['grade_id','=',$data['grade_id']];
        }

        if(!empty($data['type']) && is_numeric($data['type']) && $data['type']==1){
            $courses=ClassHour::where($wheres)->paginate('',false,['path'=>"javascript:getCours([PAGE])"]);

            return view('',compact('courses'));
        }


        if(!empty($data['subject_id']) && is_numeric($data['subject_id'])){
            $wheres[]=['subject_id','=',$data['subject_id']];
        }

        if(!empty($data['season_id']) && is_numeric($data['season_id'])){
            $wheres[]=['season_id','=',$data['season_id']];
        }


        $courses=CourseModel::where($wheres)->paginate('',false,['path'=>"javascript:getCours([PAGE])"]);



        return view('',['courses'=>$c,'b'=>$courses]);
    }
}