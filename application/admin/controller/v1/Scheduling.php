<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/6/13
 * Time: 下午2:58
 */

namespace app\admin\controller\v1;
use app\model\Teacher;
use think\helper\Time;
use think\Validate;
use app\model\Schedule;
use app\model\ClassHour ;
use app\model\Subject;
use app\model\Course as CourseModel;
use app\model\Grade as GradeModel;
class Scheduling extends BaseController
{
    /**
     * 首页
     */
    public function index(){
        $this->method();
        $this->auth();
        $subjects=Subject::all();
        $grades=GradeModel::all();
        $wheres[]=['on_line',"=",1];
        $appends=[];
        if(is_numeric(request()->get('grade_id'))){
            $wheres[]=['grade_id','=',request()->get('grade_id')];
            $appends['grade_id']=request()->get('grade_id');
        }

        if(is_numeric(request()->get('subject_id'))){
            $wheres[]=['subject_id','=',request()->get('subject_id')];
            $appends['subject_id']=request()->get('subject_id');

        }


        if(!empty(request()->get('teacher'))){
           $teacher_ids=Teacher::where('name','like','%'.request()->get('teacher').'%')->column('id');
           $wheres[]=['class_teacher','in',$teacher_ids];
           $appends['teacher']=request()->get('teacher');

        }

        if(is_numeric(request()->get('status'))){
            $wheres[]=['status','=',request()->get('status')];
            $appends['status']=request()->get('status');

        }

        if(!empty(request()->get('start_time'))){
            $wheres[]=['start_time','>=',request()->get('start_time')];
            $appends['start_time']=request()->get('start_time');


        }

        if(!empty(request()->get('end_time'))){
            $wheres[]=['end_time','>=',request()->get('end_time')];
            $appends['end_time']=request()->get('end_time');


        }

        if(is_numeric(request()->get('mode'))){
            switch(request()->get('mode')){
                case 0:
                    $wheres[]=['course_id','NotNull',''];

                    break;
                case 1:
                    $wheres[]=['class_hour_id','NotNull',''];
                    break;
            }

            $appends['mode']=request()->get('mode');

        }

        $schedules=Schedule::where($wheres)->paginate(10);
        $schedules->appends($appends);
        return view('',compact('schedules','grades','subjects'));

    }

    /**
     * 线上班级课
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function class_on_line(){
        $this->method();
        $this->auth();
        $courses=CourseModel::all();
        $grades=GradeModel::all();
        $subjects=Subject::all();
        $teachers=Teacher::all();


        return view('',compact('courses','grades','subjects','teachers'));
    }


    public function getCourseAjax(){
        $this->method();
        $this->auth();
        $id=request()->get('id');
        $course=CourseModel::with(['grade','subject'])->where('id',$id)->find();
        if(empty($course)){
            ajax_error('',"获取失败");
        }
        ajax_success($course,'获取数据成功');

    }


    /**
     * 线上班级课ajax
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function class_on_line_ajax(){
        $this->auth();
        $this->method();
        $wheres=[];
        $wheres1=[];
        $name=request()->get('name','');
        $type=request()->get('type','');
        $mode=request()->get('mode','');
        $grade=request()->get('grade','');
        $subject=request()->get('subject','');
        $level=request()->get('level','');
        $week=request()->get('week','');
        $time_type=request()->get('time_type','');
        if(!empty($name)){
            $wheres[]=['name','like',"%".$name."%"];
        }

        if(!(empty($type) && !is_numeric($type))){
            if($type!=2){
                $wheres[]=['type','in','2,'.$type];

            }else{
                $wheres[]=['type','in',$type.",2"];


            }
        }
        if(!(empty($mode) && !is_numeric($mode))){
            if($mode!=2){
                $wheres[]=['mode','in','2,'.$mode];

            }else{
                $wheres[]=['type','in',$mode.",2"];

            }
        }

        if(!(empty($grade) && !is_numeric($grade))){
            $wheres[]=['grade','=',$grade];
        }

        if(!(empty($subject) && !is_numeric($subject))){
            $wheres[]=['subject','=',$subject];
        }

        if(!(empty($level) && !is_numeric($level))){
            $wheres[]=['level','=',$level];
        }

        if(!(empty($time_type) && !is_numeric($time_type))){
            $wheres1[]=['type','=',$time_type];
        }

        $day=date('w')-1;

        $nextTime=[date("Y-m-d H:i:s",nextTime()[0]),date("Y-m-d H:i:s",nextTime()[1])];
        $weekTime=[date("Y-m-d H:i:s",Time::week()[0]+60*60*24*$day),date("Y-m-d H:i:s",Time::week()[1])];
        if(!(empty($week) && !is_numeric($week))){
            switch ($week){
                case 0:
                    $wheres1[]=['specific_date','between time',$weekTime];
                    break;
                case 1:
                    $wheres1[]=['specific_date','between time', $nextTime];
                    break;
            }
        }


        $teachers=Teacher::where($wheres)->with(['getInfo','getSubject','freeTime'=>function($query)use($wheres1){
            $query->where($wheres1);
        }])->select();

        if($teachers->isEmpty()){
            ajax_error($teachers,'没有数据');
        }
        ajax_success($teachers,'获取数据');
    }



    public function add_class_on_line(){
        $this->method('post');
        $this->auth();
        $data=request()->post();
        $validate=Validate::make([
            'course_id'=>'require',
            'live_time'=>'require',
            'start_time'=>'require',
            'end_time'=>'require',
            'class_teacher'=>'require',
            'tutor'=>'require',
            'teaching_assistant'=>'require',
            'student_number'=>'require',
            'teacher_salary'=>'require',
            'student_hour'=>'require',
            'teacher_hours'=>'require'
        ],[],[
            'course_id'=>"课程",
            'live_time'=>'直播时间',
            'start_time'=>'开课时间',
            'end_time'=>'结束时间',
            'class_teacher'=>'上课老师',
            'tutor'=>'巡课',
            'teaching_assistant'=>'助教教师',
            'student_number'=>'上课人数',
            'teacher_salary'=>'教师结算课酬',
            'student_hour'=>"学生次数课消",
            'teacher_hours'=>"教师结算课时数"
        ]);
        if(!$validate->check($data)){
            ajax_error('',$validate->getError());
        }
        $course=CourseModel::get($data['course_id']);
        $res=$this->ex_roomcreate($course->name,strtotime($data['start_time']),strtotime($data['end_time']),$data['student_number']);
        $res=json_decode($res,true);

        if($res['result']!=0){
            ajax_error('',"排课失败");
        }

        $data['room_number']=$res['serial'];
        $data['status']=1;
        if(!$d=Schedule::create($data)){

            ex_roomdelete($res['serial']);
            ajax_error('',"排课失败");
        }

        ajax_success($d,'排课成功');


    }




    /**
     * 面授
     */
    public function class_under_line(){
        $this->auth();
        $this->method();
        $grades=GradeModel::select();
        $subjects=Subject::select();
      /*  foreach ($schedules as $key => $value) {
            if($value['course_id']){
                $course=CourseModel::where('id',$value['course_id'])->find();
                $schedules[$key]['Coures_name']=$course['name'];
                $schedules[$key]['grade']=GradeModel::where('id',$course['grade_id'])->find();
                $schedules[$key]['subject']=Subject::where('id',$course['subject_id'])->find();
            }else{
                $course=ClassHour::where('id',$value['class_hour_id'])->find();
                $schedules[$key]['Coures_name']=$course['name'];
                $schedules[$key]['grade']=GradeModel::where('id',$course['grade_id'])->find();
                $schedules[$key]['subject']=Subject::where('id',$value['subject_id'])->find();
            }

        }pengyue*/
        $wheres[]=['on_line',"=",0];
        $appends=[];
        // if(request()->isGet()){
            if(is_numeric(request()->get('grade_id'))){
                $wheres[]=['grade_id','=',request()->get('grade_id')];
                $appends['grade_id']=request()->get('grade_id');
            }
    
    
            if(!empty(request()->get('teacher'))){
                $teacher_ids=Teacher::where('name','like','%'.request()->get('teacher').'%')->column('id');
                $wheres[]=['class_teacher','in',$teacher_ids];
                $appends['teacher']=request()->get('teacher');
    
            }
    
            if(is_numeric(request()->get('status'))){
                $wheres[]=['status','=',request()->get('status')];
                $appends['status']=request()->get('status');
    
            }
    
            if(!empty(request()->get('start_time'))){
                $wheres[]=['start_time','>=',request()->get('start_time')];
                $appends['start_time']=request()->get('start_time');
    
    
            }
    
            if(!empty(request()->get('end_time'))){
                $wheres[]=['end_time','>=',request()->get('end_time')];
                $appends['end_time']=request()->get('end_time');
    
    
            }
    
            if(is_numeric(request()->get('mode'))){
                switch(request()->get('mode')){
                    case 0:
                        $wheres[]=['course_id','NotNull',''];
    
                        break;
                    case 1:
                        $wheres[]=['class_hour_id','NotNull',''];
                        break;
                }
    
                $appends['mode']=request()->get('mode');
    
            }

        $schedules=Schedule::where($wheres)->paginate(10);
        $schedules->appends($appends);



        return view('',compact('schedules','grades','subjects'));
    }

    /**
     * 面授 新增班级课排课
     */
    public function class_under_line_add(){
        $this->method();
        $this->auth();
        $courses=CourseModel::all();
        $grades=GradeModel::all();
        $subjects=Subject::all();
        $teachers=Teacher::all();
        return view('',compact('courses','grades','subjects','teachers'));
    }

    /**
     * 面授 新增1v1排课
     */
    public function class_hour_under_line_add(){
        $this->method();
        $this->auth();
        $courses=CourseModel::all();
        $grades=GradeModel::all();
        $subjects=Subject::all();
        $teachers=Teacher::all();
        return view('',compact('courses','grades','subjects','teachers'));
    }


    public function class_hour_on_line_add(){
        $this->method();
        $this->auth();
//        $class_hours=ClassHour::all();
        $grades=GradeModel::all();
        $subjects=Subject::all();
        $teachers=Teacher::all();
        return view('',compact('grades','subjects','teachers'));

    }


    
    public function class_hour(){
        $this->method();
        $this->auth();
        $id=request()->get('id');
        $class_hour=ClassHour::with('grade')->where('id',$id)->find();
        if(empty($class_hour)){
            ajax_error('',"获取失败");
        }

        ajax_success($class_hour,'获取成功');
    }



    public function add_class_hours_on_line_ajax(){
        $this->method('post');
        $this->auth();
        $data=request()->post();
        $validate=Validate::make([
            'class_hour_id'=>'require',
            'live_time'=>'require',
            'start_time'=>'require',
            'end_time'=>'require',
            'class_teacher'=>'require',
            'tutor'=>'require',
            'teaching_assistant'=>'require',
//            'student_number'=>'require',
            'teacher_salary'=>'require',
            'student_hour'=>'require',
            'teacher_hours'=>'require',
            'student_id'=>'require'
        ],[],[
            'class_hour_id'=>"课程",
            'live_time'=>'直播时间',
            'start_time'=>'开课时间',
            'end_time'=>'结束时间',
            'class_teacher'=>'上课老师',
            'tutor'=>'巡课',
            'teaching_assistant'=>'助教教师',
//            'student_number'=>'上课人数',
            'teacher_salary'=>'教师结算课酬',
            'student_hour'=>"学生次数课消",
            'teacher_hours'=>"教师结算课时数",
            'student_id'=>'学生'

        ]);
        if(!$validate->check($data)){
            ajax_error('',$validate->getError());
        }


        $course=ClassHour::get($data['class_hour_id']);
        $res=$this->ex_roomcreate($course->name,strtotime($data['start_time']),strtotime($data['end_time']),1,0);
        $res=json_decode($res,true);

        if($res['result']!=0){
            dump($res);
            ajax_error('',"排课失败");
        }

        $data['room_number']=$res['serial'];
        $data['status']=1;
        if(!$d=Schedule::create($data)){

            ex_roomdelete($res['serial']);
            ajax_error('',"排课失败");
        }

        ajax_success($d,'排课成功');

    }


    //面授1V1排课
    public function classHourUnderlinAdd(){
        $data=request()->post();
        if(!$data['student_id']){
            ajax_error('',"请选择学生");
        }
        if(!$data['class_hour_id']){
            ajax_error('',"请选择课程");
        }
        if(!$data['class_teacher']){
            ajax_error('',"请选择上课老师");
        }
        if(!$data['class_address']){
            ajax_error('',"请输入上课地址");
        }
        if(!$data['teacher_salary']){
            ajax_error('',"请输入教师结算课酬");
        }
        if(!$data['teacher_hours']){
            ajax_error('',"请输入教师结算课时数");
        }
        if(!$data['student_hour']){
            ajax_error('',"请输入学生课消费课时包");
        }
        $data['status']=1;
        $data['on_line']=0;
        $res=Schedule::insert($data);
        if($res){
            ajax_success($res,'排课成功');
        }else{
            ajax_error('',"排课失败");
        }
    }

    //面授班级课排课
    public function classUnderlinAdd(){
        $data=request()->post();
        unset($data['grade'],$data['subject']);
        if(!$data['course_id']){
            ajax_error('',"请选择课程");
        }
        if(!$data['class_address']){
            ajax_error('',"请输入上课地址");
        }
        if(!$data['class_teacher']){
            ajax_error('',"选择上课老师");
        }
        if(!$data['teacher_salary']){
            ajax_error('',"请输入教师结算课酬");
        }
        if(!$data['teacher_hours']){
            ajax_error('',"请输入教师结算课时数");
        }
        $data['status']=1;
        $data['on_line']=0;
        $res=Schedule::insert($data);
        if($res){
            ajax_success($res,'排课成功');
        }else{
            ajax_error('',"排课失败");
        }
    }

    public function cancelCourse(){
        $data=request()->post();
        $data['status']=4;
        $res=Schedule::update( $data);
        if($res){
            ajax_success($res,'取消成功');
        }else{
            ajax_error('',"取消失败");
        }
    }


    public function Schedulings($id){
        $this->method();
        $this->auth();
        $schedule=Schedule::get($id);
        $lists=Schedule::where('course_id',$schedule->course_id)->order('start_time')->select();
        if(empty($schedule)){
            $this->error('没有该课程');
        }

        return view('',compact('schedule','lists'));

    }


    /**
     * 改变状态
     */
    public function changeStatus(){
        $lives=Schedule::whereNotIn('status',[3,4])->select();
        $time=time();
        foreach ($lives as $live){
            if($time>$live->start_time && $time<$live->end_time){
                $live->save(['status'=>0]);
            }elseif($time<$live->start_time){
                $live->save(['status'=>1]);

            }else{
                $live->save(['status'=>2]);

            }
        }
    }
}