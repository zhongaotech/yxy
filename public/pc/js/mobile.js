$(".step1").click(function() {
    $(".point1").css({
        "background": "#fff",
        'color': '#202020'
    });
    $(".point2").css({
        "background": "#fff",
        'color': '#202020'
    });
    $(".point3").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".point4").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".point5").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".step1").css({
        "background-image": "linear-gradient(-180deg, rgba(255, 209, 79, 0) 17%, rgba(255, 204, 81, .8) 78%, rgba(255, 222, 89, 0));"
    });
    $(".triangle1 span").css({
        "background-color": "#fff"
    });
    $(".triangle2 span").css({
        "background-color": "#fff"
    });
    $('.person_text').eq(0).addClass('cur').siblings('.person_text').removeClass('cur');
});
$(".step2").click(function() {
    $(".point2").css({
        "background": "#fff",
        'color': '#202020'
    });
    $(".point3").css({
        "background": "#fff",
        'color': '#202020'
    });
    $(".point1").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".point4").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".point5").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".step2").css({
        "background-image": "linear-gradient(-180deg, rgba(255, 209, 79, 0) 17%, rgba(255, 204, 81, .8) 78%, rgba(255, 222, 89, 0));"
    });
    $(".triangle2 span").css({
        "background-color": "#fff"
    });
    $(".triangle3 span").css({
        "background-color": "#fff"
    });
    $('.person_text').eq(1).addClass('cur').siblings('.person_text').removeClass('cur');
});
$(".step3").click(function() {
    $(".point3").css({
        "background": "#fff",
        'color': '#202020'
    });
    $(".point4").css({
        "background": "#fff",
        'color': '#202020'
    });
    $(".point1").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".point2").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".point5").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".step3").css({
        "background-image": "linear-gradient(-180deg, rgba(255, 209, 79, 0) 17%, rgba(255, 204, 81, .8) 78%, rgba(255, 222, 89, 0));"
    });
    $(".triangle3 span").css({
        "background-color": "#fff"
    });
    $(".triangle4 span").css({
        "background-color": "#fff"
    });
    $('.person_text').eq(2).addClass('cur').siblings('.person_text').removeClass('cur');
});
$(".step4").click(function() {
    $(".point4").css({
        "background": "#fff",
        'color': '#202020'
    });
    $(".point5").css({
        "background": "#fff",
        'color': '#202020'
    });
    $(".point3").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".point1").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".point2").css({
        "background": "transparent",
        'color': '#fff'
    });
    $(".step4").css({
        "background-image": "linear-gradient(-180deg, rgba(255, 209, 79, 0) 17%, rgba(255, 204, 81, .8) 78%, rgba(255, 222, 89, 0));"
    });
    $(".triangle4 span").css({
        "background-color": "#fff"
    });
    $(".triangle5 span").css({
        "background-color": "#fff"
    });
    $('.person_text').eq(3).addClass('cur').siblings('.person_text').removeClass('cur');
});
var bullets = document.getElementById('position').getElementsByTagName('li');

var banner = Swipe(document.getElementById('mySwipe'), {
    auto: 4000,
    continuous: true,
    disableScroll: false,
    callback: function(pos) {
        var i = bullets.length;
        while (i--) {
            bullets[i].className = ' ';
        }
        bullets[pos].className = 'cur';
    }
})

//图片banner
var swiper = new Swiper('#TecImg .swiper-container', {
    on: {
        slideChangeTransitionEnd: function() {
            if (this.activeIndex == 0) {
                $('#Tec1').hide();
                $('#Tec').show();
            } else if (this.activeIndex == 1) {
                $('#Tec').hide();
                $('#Tec1').show();
            }
        },
    },
    pagination: {
        el: '#TecImg .swiper-pagination',
    },
});
//教师banner
var swiper1 = new Swiper('#Tec .swiper-container', {
    pagination: {
        el: '#Tec .swiper-pagination',
    },
});
var swiper2 = new Swiper('#Tec1 .swiper-container', {
    on: {
        init: function() {
            $('#Tec1').hide();
        }
    },
    pagination: {
        el: '#Tec1 .swiper-pagination',
    },
});
// 学生推荐
var swiper3 = new Swiper('#stuRec .swiper-container', {
    pagination: {
        el: '#stuRec .swiper-pagination',
    },
});
$(function() {

        //模拟下拉框
        $('.select input').on('click', function() {
            if ($('.select .city').is('.hide')) {
                $('.select .city').removeClass('hide');
            } else {
                $('.select .city').addClass('hide');
            }
        })
        $('.select ul li').on('click', function() {
            $('.select input').val($(this).html());
            $('.select .city').addClass('hide');
            $('.select input').css('border-bottom', '1px solid $b2b2b2');
        })
        $('.select ul li').hover(
                function() {
                    $(this).css({ 'backgroundColor': '#fd9', 'font-size': '.2rem' });
                },
                function() {
                    $(this).css({ 'backgroundColor': '#fff', 'font-size': '.2rem' });
                }
            )
            //模拟下拉框
        $('.last_select input').on('click', function() {
            if ($('.last_select .last_city').is('.hide')) {
                $('.last_select .last_city').removeClass('hide');
            } else {
                $('.last_select .last_city').addClass('hide');
            }
        })
        $('.last_select ul li').on('click', function() {
            $('.last_select input').val($(this).html());
            $('.last_select .last_city').addClass('hide');
            $('.last_select input').css('border-bottom', '1px solid $b2b2b2');
        })
        $('.last_select ul li').hover(
            function() {
                $(this).css({ 'backgroundColor': '#fff', 'font-size': '.2rem' });
            },
            function() {
                $(this).css({ 'backgroundColor': '#fff', 'font-size': '.2rem' });
            }
        )

    })
    // 弹窗
$('.aud_st').click(function() {
    $('.pohide').show();
})
$('.pop_err span').click(function() {
        $('.pohide').hide();
    })
    //阻止冒泡
$('.pop_err').click(function() {
    return false;
});
$('.pohide').click(function() {
    $('.pohide').hide();
});