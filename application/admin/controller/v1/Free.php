<?php
/**
 * User: pengyue
 */

namespace app\admin\controller\v1;
use app\model\FreeTime;
use app\model\Teacher;
use app\model\Subject;

class Free extends BaseController{

    /**
     * 教师空闲首页
     */
    public function index(){
        $this->auth();
        $teachers=Teacher::select();
        foreach ($teachers as $k => $v) {
            $arrwhere['teacher_id']=$v['id'];
            $mon['week1'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',1]])->select();
            $mon['week2'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',2]])->select();
            $mon['week3'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',3]])->select();
            $mon['week4'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',4]])->select();
            $mon['week5'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',5]])->select();
            $mon['week6'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',6]])->select();
            $mon['week7'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',0],['week','=',7]])->select();
            $teachers[$k]['mon']=$mon;
            $after['week1'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',1]])->select();
            $after['week2'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',2]])->select();
            $after['week3'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',3]])->select();
            $after['week4'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',4]])->select();
            $after['week5'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',5]])->select();
            $after['week6'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',6]])->select();
            $after['week7'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',1],['week','=',7]])->select();
            $teachers[$k]['after']=$after;
            $night['week1'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',1]])->select();
            $night['week2'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',2]])->select();
            $night['week3'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',3]])->select();
            $night['week4'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',4]])->select();
            $night['week5'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',5]])->select();
            $night['week6'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',6]])->select();
            $night['week7'] = FreeTime::whereTime('specific_date', 'week')->where( $arrwhere)->where([['type','=',2],['week','=',7]])->select();
            $teachers[$k]['night']=$night;
        }
        $subject=Subject::select();
        return view('',compact('teachers','subject'));
    }
    
    /**
     * 添加教师空闲时间搜索
     */
    public function addFreeTime(){
        $data=request()->post();
        $StartTime=substr($data['FreeStart'],0,2)  ;
        $EndTime=substr($data['FreeEnd'],0,2);
        if(!$data['FreeTime']){
            ajax_error($data,'请选择时间');
        }
        if(!$data['teacher_id']){
            ajax_error($data,'请选择老师');
        }
        if($StartTime>$EndTime){
            ajax_error($data,'请选择正确时间区间');
        }
        if( ($EndTime-$StartTime)<2){
            ajax_error($data,'最小时间区间为2小时');
        }
        if(
            (($StartTime >=8 and  $EndTime >=8) and ($StartTime<=12 and $EndTime<=12)) ||
            (($StartTime >=8 and  $EndTime >=8) and ($StartTime<=18 and $EndTime<=18)) ||
            (($StartTime >=18 and  $EndTime >=18) and ($StartTime<=24 and $EndTime<=24))
        ){
            if(($StartTime >=8 and  $EndTime >=8) and ($StartTime<=12 and $EndTime<=12)){
                $FreeTime['type']=0;
            }
            if(($StartTime >=12 and  $EndTime >=12) and ($StartTime<=18 and $EndTime<=18)){
                $FreeTime['type']=1;
            }
            if(($StartTime >=18 and  $EndTime >=18) and ($StartTime<=24 and $EndTime<=24)){
                $FreeTime['type']=2;
            }
        }else{
            ajax_error($data,'请选择正确时间区间');
        }
        $FreeTime['week'] = date('w',strtotime($data['FreeTime']));
        $FreeTime['hour']=$StartTime.':00'.'-'.$EndTime.':00';
        $FreeTime['teacher_id']=$data['teacher_id'];
        $FreeTime['specific_date']=$data['FreeTime'];
        $arr=FreeTime::insert($FreeTime);
        if($arr){
            ajax_success($arr,'添加成功');
        }else{
            ajax_error($arr,'添加失败');
        }
        print_r($FreeTime);
    }

    /**
     * 教师空闲时间搜索
     */
    public function select(){
        $data=request()->post();
        $arrwhere2=[];
        $arrwhere3=[];
        if($data['grade']!=500){
            $arrwhere2['grade']=$data['grade'];
        }
        if($data['subject']!=500){
            $arrwhere2['subject']=$data['subject'];
        }
        if($data['type']!=500){
            $arrwhere2['type']=$data['type'];
        }
        if($data['mode']!=500){
            $arrwhere2['mode']=$data['mode'];
        }
        if($data['level']!=500){
            $arrwhere2['level']=$data['level'];
        }
        if($data['time']==0){
            $timewhere='last week';
        }
        if($data['time']==500){
            $timewhere='week';
        }
        if($data['time']==1){
            $timewhere='+1 week';
        }
        $teachers=Teacher::where($arrwhere2)->where("name","like",'%'.$data['name'].'%')->select();
        foreach ($teachers as $k => $v) {
            $arrwhere['teacher_id']=$v['id'];
            $mon['week1'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',0],['week','=',1]])->select();
            $mon['week2'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',0],['week','=',2]])->select();
            $mon['week3'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',0],['week','=',3]])->select();
            $mon['week4'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',0],['week','=',4]])->select();
            $mon['week5'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',0],['week','=',5]])->select();
            $mon['week6'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',0],['week','=',6]])->select();
            $mon['week7'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',0],['week','=',7]])->select();
            $teachers[$k]['mon']=$mon;
            $after['week1'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',1],['week','=',1]])->select();
            $after['week2'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',1],['week','=',2]])->select();
            $after['week3'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',1],['week','=',3]])->select();
            $after['week4'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',1],['week','=',4]])->select();
            $after['week5'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',1],['week','=',5]])->select();
            $after['week6'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',1],['week','=',6]])->select();
            $after['week7'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',1],['week','=',7]])->select();
            $teachers[$k]['after']=$after;
            $night['week1'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',2],['week','=',1]])->select();
            $night['week2'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',2],['week','=',2]])->select();
            $night['week3'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',2],['week','=',3]])->select();
            $night['week4'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',2],['week','=',4]])->select();
            $night['week5'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',2],['week','=',5]])->select();
            $night['week6'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',2],['week','=',6]])->select();
            $night['week7'] = FreeTime::whereTime('specific_date', $timewhere)->where( $arrwhere)->where([['type','=',2],['week','=',7]])->select();
            $teachers[$k]['night']=$night;
        }
        
        $subject=Subject::select();
        return view('',compact('teachers'));
    }

    /**
     * 编辑空闲时间
     */
    public function freeEdit($id){
        $this->auth();
        $teacher=Teacher::get($id);
        $arrwhere['teacher_id']=$id;
        $freeTime=[];
        // 数组拼接
        $week1['mon']   = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',0],['week','=',1]])->select();
        $week1['after'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',1],['week','=',1]])->select();
        $week1['night'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',2],['week','=',1]])->select();
        $week1['week']='一';
        $week1['data']=date('Y-m-d', (time() - ((date('w') == 0 ? 7 : date('w')) - 1) * 24 * 3600));

        $week2['mon']   = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',0],['week','=',2]])->select();
        $week2['after'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',1],['week','=',2]])->select();
        $week2['night'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',2],['week','=',2]])->select();
        $week2['week']='二';
        $week2['data']=date('Y-m-d', (time() - ((date('w') == 0 ? 7 : date('w')) - 2) * 24 * 3600));

        $week3['mon']   = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',0],['week','=',3]])->select();
        $week3['after'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',1],['week','=',3]])->select();
        $week3['night'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',2],['week','=',3]])->select();
        $week3['week']='三';
        $week3['data']=date('Y-m-d', (time() - (3-(date('w') == 0 ? 7 : date('w')) - 3) * 24 * 3600));

        $week4['mon']   = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',0],['week','=',4]])->select();
        $week4['after'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',1],['week','=',4]])->select();
        $week4['night'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',2],['week','=',4]])->select();
        $week4['week']='四';
        $week4['data']=date('Y-m-d', (time() - (4-(date('w') == 0 ? 7 : date('w')) - 4) * 24 * 3600));

        $week5['mon']   = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',0],['week','=',5]])->select();
        $week5['after'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',1],['week','=',5]])->select();
        $week5['night'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',2],['week','=',5]])->select();
        $week5['week']='五';
        $week5['data']=date('Y-m-d', (time() - (5-(date('w') == 0 ? 7 : date('w')) - 5) * 24 * 3600));

        $week6['mon']   = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',0],['week','=',6]])->select();
        $week6['after'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',1],['week','=',6]])->select();
        $week6['night'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',2],['week','=',6]])->select();
        $week6['week']='六';
        $week6['data']=date('Y-m-d', (time() - (6-(date('w') == 0 ? 7 : date('w')) - 6) * 24 * 3600));

        $week7['mon']   = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',0],['week','=',7]])->select();
        $week7['after'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',1],['week','=',7]])->select();
        $week7['night'] = FreeTime::whereTime('specific_date','week')->where( $arrwhere)->where([['type','=',2],['week','=',7]])->select();
        $week7['week']='日';
        $week7['data']=date('Y-m-d', (time() - (7-(date('w') == 0 ? 7 : date('w')) - 1) * 24 * 3600));

        $freeTimes['week1']=$week1;
        $freeTimes['week2']=$week2;
        $freeTimes['week3']=$week3;
        $freeTimes['week4']=$week4;
        $freeTimes['week5']=$week5;
        $freeTimes['week6']=$week6;
        $freeTimes['week7']=$week7;

        return view('',compact('teacher','freeTimes'));
    }


    /**
     * 删除时间
     */
    public function deleteTime($id)
    {   
        $data=FreeTime::destroy($id);
        if($data){
            ajax_success($data,'删除成功');
        }else{
            ajax_error($data,'删除失败');
        }
    }

    /**
     * 修改时间
     */
    public function editTime()
    {   
        $data=request()->post();
        $StartTime=substr($data['FreeStart'],0,2)  ;
        $EndTime=substr($data['FreeEnd'],0,2);
        
        if($StartTime>$EndTime){
            ajax_error($data,'请选择正确时间区间');
        }
        if( ($EndTime-$StartTime)<2){
            ajax_error($data,'最小时间区间为2小时');
        }
        if(
            (($StartTime >=8 and  $EndTime >=8) and ($StartTime<=12 and $EndTime<=12)) ||
            (($StartTime >=8 and  $EndTime >=8) and ($StartTime<=18 and $EndTime<=18)) ||
            (($StartTime >=18 and  $EndTime >=18) and ($StartTime<=24 and $EndTime<=24))
        ){
            if(($StartTime >=8 and  $EndTime >=8) and ($StartTime<=12 and $EndTime<=12)){
                $FreeTime['type']=0;
            }
            if(($StartTime >=12 and  $EndTime >=12) and ($StartTime<=18 and $EndTime<=18)){
                $FreeTime['type']=1;
            }
            if(($StartTime >=18 and  $EndTime >=18) and ($StartTime<=24 and $EndTime<=24)){
                $FreeTime['type']=2;
            }
        }else{
            ajax_error($data,'请选择正确时间区间');
        }
        $FreeTime['hour']=$StartTime.':00'.'-'.$EndTime.':00';
        $arr=FreeTime::where('id',$data['id'])->update($FreeTime);

        if($data){
            ajax_success($data,'修改成功');
        }else{
            ajax_error($data,'修改失败');
        }
    }
}