<?php
/**
 * Created by Subline.
 * User: weilang
 * Date: 2018/6/11
 * Time: 19:59
 */

namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;

class Transfer extends Model{
    use SoftDelete;
    protected $deleteTime = 'del_time';
    protected $table='yxy_transfer';
}