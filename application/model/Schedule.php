<?php
/**
 * User: pengyue
 * Date: 2018/6/12
 */
namespace app\model;
use think\Model;

class Schedule extends Model
{   

    protected $table = 'yxy_schedule';

    public function getCourse(){
        return $this->belongsTo(Course::class,'course_id','id')->useSoftDelete('id',['not null','']);
    }

    public function getClassHour(){
        return $this->belongsTo(ClassHour::class,'class_hour_id','id')->useSoftDelete('id',['not null','']);
    }

    public function getGrade(){
        return $this->belongsTo(Grade::class,'grade_id','id');
    }
    public function getSubject(){
        return $this->belongsTo(Subject::class,'subject_id','id');
    }
    public function getStudent(){
        return $this->belongsTo(User::class,'student_id','id');
    }

    public function getTeacher(){
        return $this->belongsTo(Teacher::class,'class_teacher','id');
    }


    public function getAssistant(){
        return $this->belongsTo(Teacher::class,'teaching_assistant','id');
    }
}