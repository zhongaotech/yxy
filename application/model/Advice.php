<?php
/**
 * Created by PhpStorm.
 * User: weilang
 * Date: 2018/4/11
 * Time: 15:50
 */
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;

class Advice extends Model{
	
    protected $name='advice';
    use SoftDelete;
    protected $deleteTime = 'del_time';


}