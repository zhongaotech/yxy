<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/5/14
 * Time: 上午9:49
 */

namespace app\admin\controller\v1;
use app\model\Advice as AdviceModel;
class Advice extends BaseController
{
    public function index(){
        $this->method();
        $this->auth();
        $advices=AdviceModel::paginate();
        return view('',compact('advices'));
    }
}