<?php
/**
 * Created by PhpStorm.
 * User: weilang
 * Date: 2018/4/11
 * Time: 15:50
 */
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;

class Special extends Model{
	
    protected $name='special';
    use SoftDelete;
    protected $deleteTime = 'del_time';

    public function getPictureAttr($value){
        return url($value,'',false,true);
    }

    public function setContentAttr($value){
        return htmlspecialchars($value);
    }

    public function getContentAttr($value){
        return htmlspecialchars_decode($value);
    }


}