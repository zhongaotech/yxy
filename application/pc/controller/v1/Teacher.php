<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/5/16
 * Time: 下午3:09
 */

namespace app\pc\controller\v1;


use app\model\Area;
use app\model\City;
use app\model\ClassHour;
use app\model\Evaluate;
use app\model\EvaluateType;
use app\model\Order;
use app\model\Problem;
use app\model\Province;
use app\model\Schedule;
use app\model\User;
use app\model\UserInfo;
use app\pc\controller\BaseController;
use think\Validate;
use app\model\SendSms;
use app\model\ProblemType;
use app\model\Grade;
use Db;
use app\model\Teacher as TeacherModel;
class Teacher extends BaseController
{



    /**
     * 登陆视图
     * @return \think\response\View
     */
    public function login(){

        if(!empty(session('user_id','','pc'))){
            $this->success('你已经登录过','/pc/v1/member/index');
        }
        return view();
    }

    /**
     * 登录行为
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function loginPost(){
        $data=request()->post();
        $validate=Validate::make([
            'phone'=>'require|number',
            'password'=>"require"
        ],[],[
            'phone'=>"手机号",
            'password'=>"密码"
        ]);
        if(!$validate->check($data)){
            ajax_error('',$validate->getError());
        }

        $user=TeacherModel::where('phone',$data['phone'])->find();
        if(empty($user)){
            ajax_error('','用户不存在');
        }

        if(!password_verify($data['password'],$user->password)){
            ajax_error('','用户名或密码错误');
        }



        session('teacher_id',$user->id,'pc');
//        session('username',$user->username,'pc');

        ajax_success($user,'登录成功');
    }


    /**
     * 退出登录
     */
    public function logout(){

        session(null,'pc');
        $this->success('退出登录成功','/pc/v1/index/index');
    }


    /**
     * 首页
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function index(){
        $this->TeacherAuth();
        $this->method();
        $user_info=UserInfo::get($this->user->id);
        /**
         * 获得学籍天数
         */
        $day=ceil((time()-strtotime($this->user->create_time))/60/60/24);
        /**
         * 班级课时
         */
        $courses=$this->user->course();
        $course_hours=$courses->sum('hours');
        /**
         * 1v1课时
         */
        $class_hours=$this->user->classHour();
        $c_hours=$class_hours->sum('hours');

        /**
         * 累计课时
         */
        $hours=$course_hours+$c_hours;

        /**
         * 剩余1v1课时
         */
        $s_hours=$courses->where('type','2')->sum('hours');
        $surplus=$c_hours-$s_hours;

        return view('',compact('user','user','day','class_hours','c_hours','hours','s_hours','surplus','user_info'));
    }

    public function info(){
        $this->method();
        $this->auth();
        return view();
    }


    public function infoPost(){
        $this->method('post');
        $this->auth();
        $data=request()->post();
        $imgs=uploadImageOne('img',4);
        if($imgs['status']==0){
            $data['user']['img']=$imgs['message'];
        }
        if(!empty($data['userInfo']['area'])){
            $data['userInfo']['province']=$data['province'];
            $data['userInfo']['city']=$data['city'];
            $data['userInfo']['area']=$data['district'];
        }
        Db::startTrans();
        try{
            $this->user->save($data['user']);
            if(!empty($data['userInfo'])){
                $this->user->UserInfo->save($data['userInfo']);
            }
            Db::commit();
            ajax_success('','修改成功');


        }catch (\Exception $e){
            Db::rollback();
            ajax_error('','修改失败');

        }

    }

    public function changePassword(){
        $this->method();
        $this->auth();
        return view();
    }


    /**
     * 修改密码
     * @return \think\response\Json
     * @throws \think\exception\DbException
     */
    public function changePasswordPost(){
        $this->auth();
        $this->method('post');
        $data=request()->post();
        $validate=Validate::make([
            'old_password'=>"require",
            'password'=>"require|confirm|unique:user",
        ],[
            'password.confirm'=>"两次密码不一致",
            'password.unique'=>"新密码和旧密码不能一样"
        ],[
            "old_password"=>'原密码',
            'password'=>"新密码"
        ]);
        $user=User::get($this->user->id);

        if(!$validate->check($data)){
            return ajax_error([],$validate->getError());
        }

        if(!password_verify(request()->post('old_password'),$user->password)){
            return ajax_error([],"原始密码不对");
        }

        if(password_verify(request()->post('password'),$user->password)){
            return ajax_error([],"不能和原密码相同");
        }

        if(!$user->save($data)){
            return ajax_error([],'修改密码');
        };



        return ajax_success([],"修改密码成功,下次登录使用新密码");
    }


    public function order(){
        $this->method();
        $this->auth();

        return view('');
    }

    public function orderAjax(){
        $this->method();
        $this->auth();
        $wheres[]=['user_id','=',$this->user->id];
        if(!is_null(request()->get('status')) && is_numeric(request()->get('status'))){
            $wheres[]=['status','=',request()->get('status')];
        }

        $orders=Order::where($wheres)->paginate(4,false,['path'=>"javascript:content([PAGE],".request()->get('status','null').");"]);
        return view('',compact('orders'));
    }

    public function orderContent(){
        $this->method();
        $this->auth();
        $id=request()->get('id');
        if(empty($id)){
            $this->error('参数错误');
        }

        $order=Order::get($id);
        if(empty($order)){
            $this->error('没有该订单');
        }

        return view('',compact('order'));
    }


    public function register(){
        return view();
    }

    public function registerPost(){
        $this->method('post');
        $data=request()->post();
        $userModel=new User();
        $validate=Validate::make([
            'phone'=>"require|unique:user",
            'password'=>"require|confirm",
            'code'=>'require',
            'username'=>"require",
        ],[
            'password.confirm'=>"两次输入密码不一致"
        ],[
            'phone'=>'手机号',
            'password'=>'密码',
            'code'=>'验证码',
            'username'=>'真实姓名'
        ]);
        if(!$validate->check($data)){
            return ajax_error([],$validate->getError());
        }
        $data1=request()->except('phone,password');
        //学籍号生成
        $number_nowday = substr(date('Ymd'),2,6);
        $number_user_max_all= User::max('number');
        $number_user_max    = substr($number_user_max_all,2,6);
        $number_user_now_max= $number_nowday.'00000';
        $number = $number_user_max_all > $number_user_now_max ? $number_user_max_all+1 : $number_nowday.'00001';
        $data1['number'] = $number;//学籍号-18042300001（年月日+排序）
        //类型type
        $data1['type'] = 0;

        $wheres[]=['phone','=',$data['phone']];
        $wheres[]=['code','=',$data['code']];
        $wheres[]=['is_use','=',0];
        $wheres[]=['code_out_time','>= time',date("Y-m-d H:i:s",time())];
        $sms=SendSms::where($wheres)->find();

        if(empty($sms)){
            return ajax_error([],'验证码已失效');
        }

        /**
         * 开启事物
         */
        Db::startTrans();
        try{
            $data1['last_login_ip']=request()->ip();
            $data2=array_merge($data,$data1);
//           'last_login_ip'=>request()->ip()
            $user=User::create($data2);

//            $sms->save(['is_use'=>1]);

            /**
             * 事务运行
             */
            Db::commit();
            return ajax_success(compact('user','token'),"注册成功");
        }catch (\Exception $e){
            /**
             * 事务回滚
             */
            Db::rollback();
            return ajax_error([],$e->getMessage());

        }

    }


    /**
     * @param $phone 手机号
     * @param int $length 验证码长度
     * @param int $out_time 过期时间
     * @return \think\response\Json|void
     */
    public function sendSms()
    {
        $this->method('post');
        $data=request()->post();

        $validate=Validate::make([
            'phone'=>"require",
        ],[],[
            'phone'=>'手机号',
        ]);
        if(!$validate->check($data)){
            return ajax_error([],$validate->getError());
        }



        sendSms($data['phone'], 4, 60);
    }

    /**
     *联系客服
     */
    public function problem(){
        $this->method();
        $this->auth();
        $types=ProblemType::order('listor','asc')->select();
        return view('',compact('types'));

    }

    /**
     * 联系客服ajax
     * @throws \think\exception\DbException
     */
    public function problemAjax(){
        $this->method();
        $this->auth();
        $wheres=[];
        if(!is_null(request()->get('type')) && is_numeric(request()->get('type'))){
            $wheres[]=['type_id','=',request()->get('type')];
        }

        $problems=Problem::where($wheres)->paginate(10,false,['path'=>"javascript:problems([PAGE],".request()->get('type','null').");"]);
        return view('',compact('problems'));
    }


    public function feedback(){
        $this->method();
        $this->auth();
        $types=EvaluateType::order('listor')->select();
        $evaluates=Evaluate::order("create_time")->paginate(1);
        return view('',compact('types','evaluates'));
    }


    public function feedback_my(){
        $this->method();
        $this->auth();
        $types=EvaluateType::order('listor')->select();
        $evaluates=Evaluate::order("create_time")->select();
        // dump($evaluates);
        return view('',compact('types','evaluates'));
    }


    public function feedbackPost(){
        $this->method('post');
        $this->auth();

        $evaluate=array();
        $data=request()->post();
        $validate=Validate::make([
            'type'=>"require",
            'content'=>'require'
        ],[],[
            'type'=>"问题类型",
            'content'=>"问题内容"
        ]);

        if(!$validate->check($data)){
            return ajax_error([],$validate->getError());
        }

        $ProblemType=EvaluateType::get($data['type']);
        if(empty($ProblemType)){
            return ajax_error([],"问题类型不存在");

        }

        $evaluate['type_id']=$data['type'];
        $evaluate['content']=$data['content'];

        $img=uploadsImg('files',1024,1024);

        if($img['status']==0){
            $evaluate['picture']=$img['message'];
        }

        $evaluate['user_id']=$this->user->id;

        $evaluateObject=Evaluate::create($evaluate);

        if(!$evaluateObject->id){
            ajax_error([],'反馈失败');
        }
        ajax_success([],'反馈成功');

    }


    /**
     * 课时包充值
     * @return \think\response\View
     */
    public function recharge(){
        $this->auth();
        $this->method();
        $grades=Grade::order('listor','desc')->select();
        return view('',compact('grades'));
    }

    public function rechargePost(){
        $this->auth();
        $this->method();
        $where=[];
        if(!empty(request()->get("grade_id")) && is_numeric(request()->get("grade_id"))){
            $where['grade_id']=request()->get("grade_id");
        }
        $hours=ClassHour::where($where)->paginate('',false,['path'=>"javascript:course([PAGE],".request()->get('grade_id','null').");"]);
        return view('',compact('hours'));
    }

    public function course(){
        $this->TeacherAuth();
        $this->method();
        return view();
    }

    /**
     *班级课
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function myCourse(){
        $this->TeacherAuth();
        $this->method();
        $course_ids=Schedule::where('class_teacher',$this->teacher->id)->group(['course_id'])->column('course_id');
        $courses=\app\model\Course::whereIn('id',$course_ids)->paginate(4,false,['path'=>"javascript:myCourse([PAGE]);"]);
//        $courses=TeacherModel:: get($this->teacher->id)->course()->paginate(4,false,['path'=>"javascript:myCourse([PAGE]);"]);
        foreach ($courses as $cours){
           $cours->complete=$cours->getLive()->where('status',2)->sum('teacher_hours');
        }
//        dump($courses);
        return view('',compact('courses'));
    }



    /**
     *1v1
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function myClassHour(){
        $this->TeacherAuth();
        $this->method();
        $class_hour_ids=Schedule::where('class_teacher',$this->teacher->id)->group(['class_hour_id'])->column('class_hour_id');

        $classHour=ClassHour::whereIn('id',$class_hour_ids)->paginate(4,false,['path'=>"javascript:myClassHour([PAGE]);"]);
        foreach ($classHour as $hour){
            $hour->complete=$hour->getLive()->where('status',2)->sum('teacher_hours');

        }
        return view('',compact('classHour'));
    }


    public function liveDesc(){
        $this->TeacherAuth();
        $this->method();
        $course_id=request()->get("course_id");
        $class_hour_id=request()->get("class_hour_id");
        if(!empty($course_id)){
            $course=\app\model\Course::with(['getLive'=>function($query){
                $query->where('class_teacher',$this->teacher->id)->order('create_time');
            }])->find(['id'=>$course_id]);

        }else{
            $course=ClassHour::with(['getLive'=>function($query){
                $query->where('class_teacher',$this->teacher->id)->order('create_time');
            }])->find(['id'=>$class_hour_id]);

        }

        $course->complete=$course->getLive()->where('status',2)->sum('teacher_hours');

        return view('',compact('course'));

    }


    public function comment(){
        $this->TeacherAuth();
        $this->method('post');
        $id=request()->post("id");
        if(empty($id)){
            ajax_error($this->teacher,'缺少参数');
        }

        $live=Schedule::where('id',$id)->where('class_teacher',$this->teacher->id)->find();
        if(empty($live)){
            ajax_error($this->teacher,'没有该直播');

        }
        $live->comment=request()->post('comment');
        if(!$live->save()){
            ajax_error($this->teacher,'评价失败');

        }
        ajax_success($this->teacher,'评价成功');



    }

}