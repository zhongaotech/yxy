<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/5/15
 * Time: 上午9:29
 */

namespace app\pc\controller\v1;
use app\model\Course;
use app\model\Grade;
use app\model\IndexImage;
use app\model\NewImg;
use app\model\News;
use app\model\NewsType;
use app\model\Subject;
use app\pc\controller\BaseController;
use think\Validate;
use app\model\User;
use Db;
use app\model\Advice;

class Index extends BaseController
{
    public function index(){
        $this->auth(false);
        $this->TeacherAuth(false);
        //首页轮播图
        $IndexImage = IndexImage::where('status',1)->where('cate',0)->limit(3)->select();
        //高中课程
        $height_schools=Grade::where('name',"like","%高%")->order('listor')->select();
        // dump($height_schools);die;
        $height_school_ids=Grade::where('name',"like","%高%")->column('id');
        //初中课程
        $junior_middle_schools=Grade::where('name',"like","%初%")->order('listor')->select();
        $junior_middle_school_ids=Grade::where('name',"like","%初%")->column('id');
        //小学课程
        $primary_school=Grade::whereNotIn('id',array_merge($height_school_ids,$junior_middle_school_ids))->select();
        //科目
        $subjects=Subject::order('listor')->select();
        //年级
        $grades=Grade::order('listor')->select();
        //教育头条图片
        $NewImg = NewImg::limit(4)->select();
        //教育头条新闻-头条推荐
        $news1 = News::where('type',1)->limit(3)->select();
        //教育头条新闻-悦学优新闻
        $news3 = News::where('type',3)->limit(3)->select();
        //教育头条新闻-学习规划
        $news4 = News::where('type',4)->limit(3)->select();
        //教育头条新闻-家长课堂
        $news5 = News::where('type',5)->limit(3)->select();
        //教育头条新闻-教育分类
        $news_cate=NewsType::where('parent_id',2)->select();


        // dump($user);
        return  view('',compact('IndexImage','NewImg','height_schools','junior_middle_schools','primary_school','subjects','grades','news1','news3','news4','news5'));

    }


    /**
     * 课程列表
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function course(){
        $this->auth(false);
        $this->TeacherAuth(false);

        $wheres=[];
        if(!empty(request()->get('subject'))  && request()->get('subject')!='小'){
            $wheres=Grade::where('name','like',"%".request()->get('subject')."%")->column('id');
        }elseif(request()->get('subject')=='小') {
        	 $height_school_ids=Grade::where('name',"like","%高%")->column('id');
            $junior_middle_school_ids=Grade::where('name',"like","%初%")->column('id');
            $wheres=Grade::whereNotIn('id',array_merge($height_school_ids,$junior_middle_school_ids))->column('id');

        }

        if(!empty(request()->get('subject'))){
            $courses=Course::whereIn('grade_id',$wheres)->where('recommend',1)->order('create_time')->limit(8)->select();
        }else{
            $courses=Course::order('create_time')->where('recommend',1)->limit(8)->select();

        }

        return view('',compact('courses'));

    }



    /**
     * 0元试听
     * @return \think\response\Json
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function registration(){
        $data=request()->post();
        $validate=Validate::make([
            'username'=>"require",
            'phone'=>"require",
            'grade'=>'require',
            'subject'=>"require"

        ],[],[
            'username'=>"姓名",
            'phone'=>"手机号",
            'grade'=>"年级",
            'subject'=>"科目"
        ]);

        if(!$validate->check($data)){
            return ajax_error('',$validate->getError());
        }
        $user=User::where('phone',$data['phone'])->find();
        if(!empty($user)){
            return ajax_error('','请不要重复提交');
        }
        $data1['grade']=Grade::where('name',$data['grade'])->value('id');
        $data['subject']=Subject::where('name',$data['subject'])->value('id');
        Db::startTrans();
        try{
            $number_nowday = substr(date('Ymd'),2,6);
            $number_user_max_all= User::max('number');
            $number_user_max    = substr($number_user_max_all,2,6);
            $number = $number_nowday > $number_user_max ? $number_nowday.'00001' : $number_user_max_all+1;
            $user=User::create(['username'=>$data['username'],'type'=>1,'subject'=>$data['subject'],'phone'=>$data['phone'],'number'=>$number]);
            $user->UserInfo()->save($data1);
            ajax_success('','添加成功');
            Db::commit();
        }catch (\Exception $e){
            ajax_error('',$e->getMessage());
            Db::rollback();
        }
    }



    /**
     * 咨询
     */
    public function advice(){
        $this->method('post');
        $data=request()->post();
        $validate=Validate::make([
            'username'=>'require',
            'phone'=>'require',
        ],[],[
            'phone'=>"手机号",
            'username'=>"姓名"
        ]);
        if(!$validate->check($data)){
            ajax_error('',$validate->getError());
        }

        if(Advice::create($data)){
            ajax_success('','提交成功');
        }
        ajax_error('','提交失败');
    }


}