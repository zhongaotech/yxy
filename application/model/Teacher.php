<?php
/**
 * User: pengyue
 * Date: 2018/6/5
 */
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;

class Teacher extends Model
{   
    use SoftDelete;
    protected $deleteTime = 'del_time';
    protected $table = 'yxy_teacher';
    protected $hidden=['password'];

    public function setPasswordAttr($value){
        return password_hash($value,PASSWORD_BCRYPT);
    }

    public function getInfo(){
        return $this->hasOne(TeacherInfo::class,'teacher_id','id');
    }

    public function getSubject(){
        return $this->belongsTo(Subject::class,'subject','id');
    }
    public function getGrade(){
        return $this->belongsTo(Grade::class,'grade','id');
    }
    public function freeTime(){
        return $this->hasMany(FreeTime::class,'teacher_id','id');
    }


    public function getEvaluateAttr($value){
        return unserialize($value);
    }

    public function getDeviceAttr($value){
        if(empty($value)){
            return $value;
        }
        return unserialize($value);
    }





}