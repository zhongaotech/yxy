<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/5/14
 * Time: 上午10:32
 */

namespace app\admin\controller\v1;
use app\model\Reported as reportedModel;
use think\Validate;

class Reported extends BaseController
{
    /**
     * 首页
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function index(){
        $this->method();
        $this->auth();
        $reporteds=reportedModel::order('listor')->paginate();
        return view('',compact('reporteds'));

    }

    public function add(){
        $this->method();
        $this->auth();
        return view();
    }

    /**
     * 编辑状态
     */
    public function addPost(){
        $this->method('post');
        $this->auth();
        $data=request()->post();
        $validate=Validate::make([
            'title'=>"require",
            'url'=>"require",
        ],[],[
            'title'=>"标题",
            'url'=>"网址"
        ]);

        if(!$validate->check($data)){
            ajax_error('',$validate->getError());
        }

        $imgs=uploadImageOne('thumb','1','jpg,png,gif,jpeg','./uploads','660','370');
        if($imgs['status']!=0){
            ajax_error('',$data['message']);
        }

        $data['thumb'] = $imgs['message'];
        $res = reportedModel::create($data);
        if($res){
            ajax_success('','添加成功');
        }else{
            ajax_error('','添加失败');
        }
    }


    /**
     * 编辑
     *
     * @param $id
     * @return \think\response\View
     * @throws \think\exception\DbException
     */
    public function edit($id){
        $this->method();
        $this->auth();
        $reported=reportedModel::get($id);
        return view('',compact('reported'));
    }


    public function editPost(){
        $this->method('post');
        $this->auth();
        $data=request()->post();
        $id=request()->post('id');
        $validate=Validate::make([
            'title'=>"require",
            'url'=>"require",
        ],[],[
            'title'=>"标题",
            'url'=>"网址"
        ]);

        $imgs=uploadImageOne('thumb','1','jpg,png,gif,jpeg','./uploads','660','370');
        if($imgs['status']==0){
            $data['thumb']=$imgs['message'];
        }

        // dump($data);die;
        if(!$validate->check($data)){
            ajax_error('',$validate->getError());
        }

        if(!$reported=reportedModel::where('id',$id)->update($data)){
            ajax_error('','修改失败');
        }
        ajax_success($reported,'修改成功');


    }


}