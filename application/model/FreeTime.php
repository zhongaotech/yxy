<?php
/**
 * User: pengyue
 * Date: 2018/6/5
 */
namespace app\model;
use think\Model;
use think\model\concern\SoftDelete;

class FreeTime extends Model
{   
    use SoftDelete;
    protected $deleteTime = 'del_time';
    protected $table = 'yxy_free_time';

    public function getTeacher(){
        return $this->belongsTo(Teacher::class,'id','teacher_id');
    }
}