/* 
 * @Author: anchen
 * @Date:   2017-06-20 16:09:23
 * @Last Modified by:   anchen
 * @Last Modified time: 2017-06-29 14:41:49
 */

$(document).ready(function() {
    $('.dh_fix').hover(function() {
        $(this).animate({ 'width': '180px' });
    }, function() {
        $(this).animate({ 'width': '60px' });
    });

    $('.tp').hover(function() {
        $(this).find('a').find('img').css('transform', 'scale(1.2)');
        $(this).find('.er_hide').show();
    }, function() {
        $(this).find('img').css('transform', 'scale(1)');
        $(this).find('.er_hide').hide();
    });


    $(window).scroll(function(event) {
        var h = $(this).scrollTop();
        //console.log(h);
        if (h > 400 && h < 1000) {
            $('.dbzf_fix').fadeIn();
        } else {
            $('.dbzf_fix').fadeOut();
        }
    });
});

/*下拉框*/
function divselect(divselectid, inputselectid) {
    var inputselect = $(inputselectid);
    $(divselectid + " cite").click(function(e) {
        var ul = $(divselectid + " ul");
        if (ul.css("display") == "none") {
            ul.slideDown("fast");
        } else {
            ul.slideUp("fast");
        }
        e.stopPropagation();
    });
    $(divselectid + " ul li a").click(function() {
        var txt = $(this).text();
        $(divselectid + " cite").html(txt);
        var value = $(this).attr("selectid");
        inputselect.val(value);
        $(divselectid + " ul").hide();

    });
    $(document).click(function() {
        $(divselectid + " ul").hide();
    });
};


// 导航
$(document).ready(function() {
    $(".user_nav ul li:eq(0)").show();
    $(".user_nav ul li").click(function() {
        $(this).addClass("activea").next("dl").slideToggle(300).siblings("dl").slideUp("slow");
        $(this).siblings().removeClass("activea");
    });
});