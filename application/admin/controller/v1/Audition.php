<?php
/**
 * Created by Sublime.
 * User: weilang
 * Date: 2018/6/12
 * Time: 20:31
 */

namespace app\admin\controller\v1;
use Request;
use app\model\Grade;
use app\model\Subject;
use app\model\Course;
use app\model\ClassHour;
use app\model\User;//操作意向用户表
use app\model\UserInfo;//操作意向用户表
use app\model\Audition as AuditionModel;//试听课表

class Audition extends BaseController{


	/**
	 * 试听课列表
	 */
    public function index(){
    	$this->method();
        $this->auth();
        $data = AuditionModel::select();
        // dump($data);die;
        $this->assign('data',$data);
        return view();
    }


    /**
	 * 新增试听课
	 */
    public function add(){
    	$this->method();
        $this->auth();
        $grades=Grade::order("listor")->select();
        $subjects=Subject::order("listor")->select();

        $this->assign('grades',$grades);
        $this->assign('subjects',$subjects);
        return view();
    }


    /**
     * 选择课程
     */
    public function addPost(){
        $this->method('post');
        $this->auth();
        $data = Request::post();
        if($data){
            // $where['subject_id']    = $data['subject'];
            $where['grade_id']      = $data['grade'];
            $where['mode']          = 1;
            // $data = ClassHour::where($where)->select();
            ajax_success($data,'试听课排课成功');
        }else{
            ajax_error($data,'试听课排课失败');
        }
    }


     /**
     * 选择课程
     */
    public function findClass(){
        $this->method();
        $this->auth();
        $data = Request::get();
        if($data){
            $where['grade_id']      = $data['grade'];
            $where['mode']          = 1;
            $data = ClassHour::where($where)->select();
            ajax_success($data,'课程匹配成功');
        }else{
            ajax_error($data,'课程匹配失败');
        }
    }


    /**
     * 选择学生
     */
    public function findStudent(){
        $this->method();
        $this->auth();
        $data = Request::get();
        if($data){
            // $where['subject_id']    = $data['subject'];
            $where['grade_id']      = $data['grade'];
            $where['mode']          = 1;
            $data = ClassHour::where($where)->select();
            ajax_success($data,'课程匹配成功');
        }else{
            ajax_error($data,'课程匹配失败');
        }
    }





}