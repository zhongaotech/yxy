<?php
/**
 * Created by PhpStorm.
 * User: shaoguo
 * Date: 2018/5/30
 * Time: 下午3:34
 */

namespace app\admin\controller\v1;


class Classroom extends BaseController
{
    public function on_line(){
        $this->method();
        $this->auth();

        return view();
    }

    public function add_on_line(){
        $url =config('kky.url');
        $postdata['key'] =config('kky.authkey');  //必填 企业id  authkey
        $postdata['name'] = 'test'; //必填 房间名称
        $postdata['chairmanpwd'] = '123456'; //必填 主席密码	必填，4=<长度<=20
        $starttime = gmmktime(); //当前时间戳
        $postdata['starttime'] = $starttime;  //必填 房间开始时间戳(精确到秒)
        $postdata['endtime'] = $starttime+24*3600; //必填 房间结束时间(精确到秒)
        $postdata['passwordrequired'] = 1; //选填 房间是否需要普通密码 0:否、1:是
        $postdata['confuserpwd'] = '123456'; //房间普通用户密码 passwordrequired = 1 时必填(4=<长度<=10)
        $postdata['isregisteruser'] = 1; //选填 必须注册用户参加 0:否、1:是
        $postdata['allowsidelinuuser'] = 1; //选填 是否允许旁听用户参加 0:否、1:是
        $postdata['sidelineuserpwd'] = '123456'; //旁听用户密码 4=<长度<=10 allowsidelineuser = 1 时必填
        $postdata['confusernum'] = 10; //必填 普通用户数
        $postdata['sidelineusernum'] = 10; //选填 旁听用户数
        $postdata['maxvideo'] = 1; //必填 最大视频数范围(0-24)
        $postdata['roominfo'] = 'text'; //选填 房间描述
        $postdata['videotype'] = 0; //选填 视频分辨率 0：流畅   1：标清 2：高清   3：超清
        $postdata['roomlayout'] = 0; //选填 房间布局参数  0：综合布局 1:视频布局2:数据布局
        $postdata['roomusertyoe'] = '10'; //选填 参会的用户类型 '10'代表Flash用户，'01'代表Ipad用户
        $postdata['call323'] = 1; //选填 呼叫323终端或者MCU  1:表示可以呼叫  0:不能
        $postdata['callphone'] = 1; //选填 呼叫电话 1:表示可以呼叫  0:不能

        $postdata['visibleroom'] = 1; //选填 房间是否公开  0:不公开  1：公开

        $resultdata = curl_post($url,$postdata);
        echo $resultdata;
    }
}